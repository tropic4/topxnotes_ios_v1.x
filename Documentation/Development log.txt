

1.6.0(7) - Lewis Garrett - Date begun work on this build: 20210708.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6 for re-entry to App Store.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 7.                //leg20210708 - TopXNotes 1.6
// Added App Group capability and experimental code.                            //leg20210708 - TopXNotes 1.6
//  Group container = "group.com.tropic4.topxnotes"
// Fix and Restore Notes tab badge value update                                 //leg20210708 - TopxNotes 1.6
// Make App Store screenshots for 5.5" and 6.5" iPhones:                        //leg20210709 - TopxNotes 1.6
//  Use Simulator iPhone 11 Pro Max 14.4 to create 6.5" screenshots.
//  Use Simulator iPhone 6s Plus 12.2 to create 6.5" screenshots.
//  Use Preview to remove alpha channel from screenshots.
//  Add folder "App Review Data" containing screenshots to repo root.
// Validate TopXNotes 1.6.0(7) Archive - no errors.                             //leg20210709 - TopXNotes 1.6
// Implement saving notepad to App Group sub directory "TopXNotes" every time   //leg20210712 - TopXNotes 1.6
//  the Model -saveData occurred.  This makes the notepad available for future
//  importation to TopXNotes2.
// TopXNotes 1.6.0(7) Waiting for Review.                                       //leg20210713 - TopXNotes 1.6
// Change compare for "TopXNotes" tab to -hasPrefix compare - more reliable.    //leg20210713 - TopXNotes 1.6
// Exported TopXNotes 1.6.0(7) as Ad Hoc build.                                 //leg20210713 - TopXNotes 1.6
// Build TopXNotes 1.6.0(7) for TestFlight and App Store.                       //leg20210713 - TopXNotes 1.6
// Submitted TopXNotes 1.6.0(7) to App Store.                                   //leg20210713 - TopXNotes 1.6
// TopXNotes 1.6.0(7) Waiting for Review.                                       //leg20210713 - TopXNotes 1.6
// The status of your app has changed to In Review.                             //leg20210714 - TopXNotes 1.6
// The status of your app has changed to Pending Developer Release.             //leg20210714 - TopXNotes 1.6
// LaunchScreen.storyboard - set icon to 1024x1024 image.                       //leg20210714 - TopXNotes 1.6
//  This change is not in 1.6.0(7) app store build - was reviewed afterwards.
// The status of your app has changed to Ready for Sale.                        //leg20210829 - TopXNotes 1.6


1.6.0(6) - Lewis Garrett - Date begun work on this build: 20210702.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6 for re-entry to App Store.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 6.                //leg20210702 - TopXNotes 1.6
// Prevent crashing bug by unwind segue from NoteView back to NotePadView if    //leg20210705 - TopXNotes 1.6
//  notepadhas been deleted or replaced by a backup.
// Validate TopXNotes 1.6.0(6) Archive - no errors.                             //leg20210706 - TopXNotes 1.6
// Build TopXNotes 1.6.0(6) for TestFlight. No warnings or errors on upload.    //leg20210706 - TopXNotes 1.6
//  Can not install builds because of "Removed from Store" status.
// Exported TopXNotes 1.6.0(6) as Ad Hoc build.                                 //leg20210707 - TopXNotes 1.6
//  Apple Configurator 2 - Can install build on iPhones.
//  Apple Configurator 2 - Can NOT install build on iPads:
//      Configurator could not install “TopXNotes” on “Lewis’ iPad 8th Gen”.
//      The device does not support this app.

1.6.0(5) - Lewis Garrett - Date begun work on this build: 20210701.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6 for re-entry to App Store.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 5.                //leg20210701 - TopXNotes 1.6
// Removed .xib-based sources from project.                                     //leg20210701 - TopXNotes 1.6
// Bug on all iPads (iOS 12, 13, and 14):  Can not edit note because background //leg20210701 - TopXNotes 1.6
//  is completely black and software keyboard won't come up.
//  Fix by copying NoteView scene from TopXNotes2 using Xcode 11.7 and changing
//  segue connections to point to it.
//
// Validate TopXNotes 1.6.0(4) Archive - no errors.                             //leg20210702 - TopXNotes 1.6
// Exported TopXNotes 1.6.0(4) as Ad Hoc build.                                 //leg20210702 - TopXNotes 1.6
//  Can not install builds on iPads using Apple Configurator 2.
// Build TopXNotes 1.6.0(4) for TestFlight. No warnings or errors on upload.    //leg20210702 - TopXNotes 1.6
//  Can not install builds because of "Removed from Store" status.


1.6.0(4) - Lewis Garrett - Date begun work on this build: 20210623.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6 for re-entry to App Store.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 4.                //leg20210623 - TopXNotes 1.6
// Missing Edit button in Note View - cannot enable editing.  Fix: Restore      //leg20210623 - TopXNotes 1.6
//  Backed-out //leg20210616 change "Force viewWillAppear on iOS 13…" in
//  NoteViewController.m -viewDidLoad, -viewDidDisappear.
// Determined -viewWillAppear not occurring problem is unsoluble with .xibs.    //leg20210624 - TopXNotes 1.6
// Begin converting TopXNotes 1.6 .xibs to .storyboard scenes:                  //leg20210625 - TopXNotes 1.6
//  A. Used xib2Storyboard.app to convert .xibs to .storyboard.
//  B. Added intial view UITabViewController to storyboard.
//  C. Using code from TopXNotes2 converted view presentations to segues.
//  D. Replaced some TopXNotes 1.6 scenes with same scenes from TopXNotes2
//     using Xcode 11.7 because Copy/Paste only worked sometimes in Xcode 12.4.
//  E. Added UITextViewDelegate -textViewDidChange so that dictation text entry
//     is not lost if no keyboard text entry.
//  F. Initialize SyncNotePad.
//  G. End conversion of .xibs to .storyboard scenes.
//
// Validate TopXNotes 1.6.0(4) Archive - no errors.                             //leg20210701 - TopXNotes 1.6
// Exported TopXNotes 1.6.0(4) as Ad Hoc build.                                 //leg20210701 - TopXNotes 1.6
// Build TopXNotes 1.6.0(4) for TestFlight. No warnings or errors on upload.    //leg20210701 - TopXNotes 1.6


1.6.0(3) - Lewis Garrett - Date begun work on this build: 20210622.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6 for re-entry to App Store.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 3.                //leg20210622 - TopXNotes 1.6
// Update the AboutViewController display.                                      //leg20210623 - TopXNotes 1.6
// Validate TopXNotes 1.6.0(3) Archive - no errors.                             //leg20210623 - TopXNotes 1.6
// Build TopXNotes 1.6.0(3) for TestFlight. No warnings or errors on upload.    //leg20210623 - TopXNotes 1.6
// TestFlight build can not update existing TopXNotes 1.5 because TopXNotes     //leg20210624 - TopXNotes 1.6
//  has been "Removed from App Store".
// Exported TopXNotes 1.6.0(3) as Ad Hoc build.  Successfully updated           //leg20210629 - TopXNotes 1.6
//  TopxNotes 1.5 on Lewis' iPhone 6+.


1.6.0(2) - Lewis Garrett - Date begun work on this build: 20210618.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6 for re-entry to App Store.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 2.                //leg20210618 - TopXNotes 1.6
// Added ITSAppUsesNonExemptEncryption - App Uses Non-Exempt Encryption = NO    //leg20210618 - TopXNotes 1.6
//    to info.plist.
// Add info.plist keys now required for WIFI access:                            //leg20210618 - TopXNotes 1.6
//  Add the 2 peer-to-peer connectivity info.plist keys that are now required
//  since Xcode 12/iOS 14:
//  Privacy - Local Network Usage Description (NSLocalNetworkUsageDescription)
//  Bonjour services (NSBonjourServices)
// Back-out //leg20210616 change "Force viewWillAppear on iOS 13…" - superceded //leg20210622 - TopXNotes 1.6
//  by //leg20210622 "Fix -viewWillAppear not occurring…" changes.
// Fix -viewWillAppear not occurring for NoteSettingsViewController and         //leg20210622 - TopXNotes 1.6
//  BackupSettingsViewController.  Also changed "Presentation" setting of Tab
//  Bar Controllers to .fullScreen.
// Validate TopXNotes 1.6.0(2) Archive - no errors.                             //leg20210622 - TopXNotes 1.6
// Build TopXNotes 1.6.0(2) for TestFlight. No warnings or errors on upload.    //leg20210622 - TopXNotes 1.6


1.6.0(1) - Lewis Garrett - Date begun work on this build: 20210614.
// Project: /Users/leg/Desktop/Git_Projects/topxnotes_ios_1.x
// Tropical Software, Inc.
//
// Purpose of project: Update TopXNotes 1.5 to 1.6.
//
// Develop Release - 1.6.0 - Build with MacOS X 10.15.7 and Xcode 12.4 / Objective-C
// TopXNotes 1.6.0 - iOS Deployment Target 9.2 - Base SDK: iOS
// Display Name: TopXNotes
// App ID: com.tropic4.TopXNotes
//
// Set Version to 1.6.0 and build number (CFBundleVersion) to 1.                //leg20210614 - TopXNotes 1.6
// Add "Access WIFI Information" to App ID com.tropic4.TopXNotes entitlements   //leg20210614 - TopXNotes 1.6
//  and turn on Automatic Signing.
// Test for iOS 4 or greater fails in -UpdateUI causing hang when NoteView is   //leg20210614 - TopXNotes 1.6
//  presented.  Commented-out test since Deployment target is now 9.2.
// Added 1024x1024 App Icon image required for App Store.                       //leg20210616 - TopXNotes 1.6
// Converted LaunchImage to LaunchScreen.storyboard.                            //leg20210616 - TopXNotes 1.6
// Force viewWillAppear on iOS 13 or greater so PickerView inited properly.     //leg20210616 - TopXNotes 1.6
// Force viewWillAppear on iOS 13 or greater so NoteView inited properly.       //leg20210616 - TopXNotes 1.6
// Force viewWillDisappear on iOS 13 or greater so NoteView cleaned-up.         //leg20210617 - TopXNotes 1.6
// Force viewWillAppear on iOS 13 or greater so SearchView inited properly.     //leg20210617 - TopXNotes 1.6
// Force viewWillAppear on iOS 13 or greater so SyncView inited properly.       //leg20210617 - TopXNotes 1.6
// Upload failed - Removed UIApplicationExitsOnSuspend key from info.plist.     //leg20210618 - TopXNotes 1.6
// Build TopXNotes 1.6.0(1) for TestFlight. No warnings or errors on upload.    //leg20210618 - TopXNotes 1.6
// TestFlight 1.6.0(1) will not install over existing App Store version of      //leg20210618 - TopXNotes 1.6
//  TopXNotes 1.5 on Lewis' iPhone 4s (iOS 9.3.6) or Lewis' iPhone 6+
//  (iOS 11.4.1):  "Could not Install TopXNotes. The requested app is not
//  available or doesn't exist."

////////////////////////////////////////////////////////////////////////////////
// Development log for TopXNotes 1.0.0 thru TopXNotes 1.5.0
////////////////////////////////////////////////////////////////////////////////

TopXNotes 

Author:  Lewis E. Garrett, 04/05/09.

Description:  An iPhone application that is a companion to the Macintosh desktop application TopXNotes.

Log: 

2018-01-11 - LEG - topxnotes_ios_v1.x (Git) (180111Develop) - Build with MacOS 10.13.2, Xcode 9.2, Base SDK iOS 8.4, Deployment iOS 8.0, current iOS 11.
	- Build 4
	- First build since 2015 to verify project builds with current environment.
	- Had to change all Xibs to build for iOS 8.3.

2015-09-24 - LEG - TopXNotes_iOS_1.5.0GM_Proj-(150924GM) - Build with MacOS 10.10.5, Xcode 6.4, Base SDK iOS 8.4, Deployment iOS 6.0.
	- Build 3
	- Had to cancel release because Apple changed rules so that the iTunes screenshots can not be changed while the app is in review or after approval.  Taking the opportunity to make a few changes.
	- Replaced hardcoded AppName with CFBundleDisplayName in several places so it easier to distinguish which TopXNotes app.
	- Changed sort button titles to reflect the current sorted state of notes.
	- Fix to insure Carriage returns are present in Email tab Emails.
	- Tagged //leg20150924 - 1.5.0

2015-03-18 - LEG - TopXNotes_iOS_1.5.0GM_Proj-(150318GM) - Build with MacOS 10.10.2, Xcode 6.2, Base SDK iOS 8.1, Deployment iOS 6.0.
	- Build 2
	- Fix for sort by title and date no longer working with iOS 8.2.  Changed sortUsingFunction comparison function prototype from static int sortByDate(Note… to NSComparisonResult sortByDate(Note…
	- Fix many Deprecation warnings.
	- Fix many "Implicit Conversion…" warnings.
	- Fix many "missing [super view…] call warnings.
	- Fix SettingsTableview so that CFBundleName is used to fill-in "About…".
	- Tagged //leg20150318 - 1.5.0

2015-03-09 - LEG - TopXNotes_iOS_1.5.0GM_Proj-(150309GM) - Build with MacOS 10.10.2, Xcode 6.1.1, Base SDK iOS 8.1, Deployment iOS 6.0.
	- Tagged //leg20150309 - 1.5.0

2014-04-17 - LEG - TopXNotes_iOS_1.5.0d1_Proj-(140417D1) - Build with MacOS 10.9.1, Xcode 5.1.1, Base SDK iOS 7.0, Deployment iOS 6.0.
	- Tagged //leg20140417 - 1.5.0.

2014-04-05 - LEG - TopXNotes_iOS_1.5.0d0_Proj-(140405D0) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 6.0.
	- Coordinated free, standard, and pro versions to 1.5.0.  Code base TopXNotes_iOS1.2.7d9_Proj-(140330D9).
	- Tagged //leg20140405 - 1.5.0.
           - Add Backup-Restore button to NoteView toolbar.
	- Change Backup-Restore icon to a hardware disk icon.

2014-03-30- LEG - TopXNotes_iOS1.2.7d9_Proj-(140330D9) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 5.0.
	- Tagged //leg20140330 - 1.2.7.
	- Created all versions of the icon.

2014-03-11- LEG - TopXNotes_iOS1.2.7d8_Proj-(140311D8) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 5.0.
	- Tagged //leg20140311 - 1.2.7.
	- Convert to ARC.

2014-02-20 - LEG - TopXNotes_iOS1.2.7d7_Proj-(140220D7) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 5.0.
	- Tagged //leg20140220 - 1.2.7.
	- Implemented "hide/show ruled notepaper background" toolbar button setting.
	- Adjust NoteView.xib so that text at end of note can be accessed and/or added-to.

2014-02-16 - LEG - TopXNotes_iOS1.2.7d6_Proj-(140216D6) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 5.0.
	- Tagged //leg20140216 - 1.2.7.
	- Implemented "hide/show ruled notepaper background" toolbar button for NoteView.
	- Remove unused images from project.  Defer using Asset Catalog until Deployment target is iOS 6.0 required.
	- Eliminate all but 2 Analyzer and Warning messages.
	- Create new iOS 7 icon with Adobe Illustrator CS4.

2014-02-12 - LEG - TopXNotes_iOS1.2.7d5_Proj-(140212D5) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 5.0.
	- Tagged //leg20140212 - 1.2.7.
	- Fixed problem of note.createDate not persisting.  createDate should now remain constant throughout the life of the note.
	- Fixed crash after HUD (font sizer slider) button is tapped the first or second time.
	- Changed HUD (font size slider) so that in addition to being hidden after 15 seconds, it is also hidden when the text view is tapped in.

2014-02-10 - LEG - TopXNotes_iOS1.2.7d4_Proj-(140210D4) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 5.0.
	- Tagged //leg20140205 - 1.2.7.
	- Update for iOS 7 is functionally complete.
	- Known problem:  The use of the font size slider inside a note view works fine for the first note that it is used in, but trying to use it in another note causes a crash.

2014-01-22 - LEG - TopXNotes_iOS1.2.7d0_Proj-(140122D0) - Build with MacOS 10.9.1, Xcode 5.0.2, Base SDK iOS 7.0, Deployment iOS 6.1.
	- Tagged //leg20140205 - 1.2.7.
	- Begin updating for iOS 7.

2014-01-16 - LEG - TopXNotes_iOS1.2.6 GM_Proj-(140116GM)  Proj - Build with MacOS 10.9.1, Xcode 4.6.3, iOS 6.0.
	- Rushed into App Review to beat the February 1, 2014 Apple deadline for rejecting Apps that aren't built with Xcode 5 and iOS 7.
	- Added 120x120 icon in response to App submission advisory Email.

2014-01-13 - LEG - TopXNotes_iOS1.2.6b10_Proj-(140113B10)
	- Add a TXT Record containing the UDID (universal identifier) of the iDevice, to the NSNetService instance (TopXNotes Sync service.)  The UDID is used by TopXNotes Mac to identify the iDevice for Sync purposes.  SyncNotePad.m, tagged //leg20140109 - 1.2.6.
	- Improved code that establishes the UDID for the iDevice.  TopXNotesAppDelegate .m, tagged //leg20140113 - 1.2.6.

2013-11-25 - LEG - TopXNotes_iOS1.2.6b9_Proj-(131204B9)
	- Fixed problem of Sync cratering because the notepad received "in one fell swoop" is too large and causes memory failure.  Fix involved breaking the reception of the notepad into small blocks.  tagged //leg20131204 - 1.2.6.

2013-11-25 - LEG - TopXNotes_iOS1.2.6b8_Proj-(131125B8) 
	- Fixed problem of Sync not re-enabling when TopXNotes becomes active again after an iOS 7 device has gone to sleep or TopXNotes has gone to the background.  Problem was caused by the NetService not having been stopped properly.
	- Tagged //leg20131125 - 1.2.6

2013-11-25 - LEG - TopXNotes_iOS1.2.6b9_Proj-(131204B9)
	- Fixed problem of Sync cratering because the notepad received "in one fell swoop" is too large and causes memory failure.  Fix involved breaking the reception of the notepad into small blocks.  tagged //leg20131204 - 1.2.6.
	- Tagged //leg20131125 - 1.2.6

2013-11-12 - LEG - TopXNotes_iOS1.2.6b7_Proj-(131112B7) 
	- Found that VERSION_NOTE_SYNC needed to be changed to 2 for the "paired" TopXNotes_Mac 1.8 / TopXNotes_iOS 1.2.6 release.
	- Fixed "notepad not sorting" after Sync.

2013-09-16 - LEG - TopXNotes_iOS1.2.6b6_Proj-(130916B6) - Build for Ad Hoc distribution.

2013-09-12 - LEG - TopXNotes_iOS1.3.0d11_Proj-(130912D11) - Merge 1.2.b6 changes into 1.3.0d10 code base.

2013-09-11 - LEG - TopXNotes_iOS1.2.6b5_Proj-(130911B5) - Build for Ad Hoc distribution.

2013-08-21 - LEG - TopXNotes_iOS1.2.6b5Proj-(130821B5) - created project from TopXNotes_touch1.2.6b4 Proj-(130801B4) Proj - Build with MacOS 10.8.4 (Mountain Lion,) Xcode-DP5, iOS 7.0.  For iOS 7.0 testing.

2013-08-01 - TopXNotes_touch1.2.6b4 Proj-(130801B4) Proj - LEG - Build with MacOS 10.8.4 (Mountain Lion,) Xcode 4.6.3., iOS 6.1.  Continued tweaking sync code.

2013-07-13 - TopXNotes_touch1.2.6b3 Proj-(130713B3) Proj - LEG - Build with MacOS 10.8.4 (Mountain Lion,) Xcode 4.6.3., iOS 6.1.  Tweak so that works reliably with Tropical Store target which has been converted to using CocoaAsyncSocket Runloop AsyncSocket.

2013-07-12 - TopXNotes_touch1.2.6b2Proj-(130712B2)  Proj - LEG - Build with MacOS 10.8.4 (Mountain Lion,) Xcode 4.6.3., iOS 6.1.  Implement an EOF marker after IOS-sent notepad file so that MacOS can detect the end of file.   GCDAsyncSocket modified TopXNotes 1.7.7b7.  Will be a paired release of TopXNotes 1.7.7 and TopXNotes_touch 1.2.6.

2013-05-13 - TopXNotes_touch1.2.6b0 Proj-(130627B0)  Proj - LEG - Build with MacOS 10.8.3 (Mountain Lion,) Xcode 4.6.3., iOS 6.1.  Begin testing GCDAsyncSocket modifications against GCDAsyncSocket modified TopXNotes 1.7.7b5.  Will be a paired release of TopXNotes 1.7.7 and TopXNotes_touch 1.2.6.

2013-05-13 - TopXNotes_touch1.2.6d0 Proj-(130513D0)  Proj - LEG - Build with MacOS 10.8.3 (Mountain Lion,) Xcode 4.6.1., iOS 6.1.  Begin implementing GCDAsyncSocket to replace existing hand coded TCP networking code.

2013-04-15 - TopXNotes_touch1.2.3 GM Proj-(130415GM)  Proj - LEG - Build with MacOS 10.8.3 (Mountain Lion,) Xcode 4.6.1., iOS 6.1.  Added VERSION_NOTE_SYNC to control "pairing-up" the MacOS and iOS versions of TopXNotes in order to prevent syncing between incompatible versions.

2013-04-11 - TopXNotes_touch1.2.3b5 GM Proj-(130411B5)  Proj - LEG - Build with MacOS 10.8.3, Xcode 4.6.1. iOS 6.1.  Added VERSION_NOTE_SYNC to control "pairing-up" the MacOS and iOS versions of TopXNotes in order to prevent syncing between incompatible versions.

2013-03-15 - TopXNotes_touch1.2.3b3 GM Proj-(130315B3)  Proj - LEG - Build with MacOS 10.8.2, Xcode 4.5.2. iOS 6.0.  Produce an interim release that improves NoteSync somewhat.  Healed some problems with nil Note.h/m field objects like noteIDs and dates.  Added DEBUG preprocessor macro to control logging during NoteSync.

2013-01-10 - TopXNotes_touch1.2.2 GM Proj-(13010GM)  Proj - LEG - Build with MacOS 10.8.2, Xcode 4.5.2. iOS 6.0.  Eliminate AddNoteViewController in favor of NoteViewController doing double duty for both Adding and Editing a note.  This is the way it should have been all along, but I didn't know how to do it when first creating TopXNotes.

2013-01-09 - TopXNotes_touch1.2.2 GM-13009GM  Proj - LEG - Build with MacOS 10.8.2, Xcode 4.5.2. iOS 6.0.  Fixing TopXNotes version not showing on iPhone 5 (Retina 4").  Change Marker Felt font to System font (Helvetica) in notepad titles (Notes, Email, Search).  Change Marker Felt font to System font (Helvetica) for Notes default font.

2013-01-04 - TopXNotes_touch1.2.2 GM-R Proj - LEG - Fix App icon on Retina iPad not showing correctly.  Fix App store submission failure due to "At least one of the following architecture(s) must be present:  armv6" with Xcode 4.5.2 no longer supporting armv6.  Set deployment target iOS to 4.3. Remove Framework libSystem.B.dylib since no longer needed for iOS 3.1.3 on first model iPhone.

2012-12-19 - TopXNotes_touch1.2.2 GM-R Proj - Re-create App store GM build.

2012-12-17 - TopXNotes_touch1.2.2 GM-R Proj - LEG - Build with MacOS 10.8.2, Xcode 4.5.2. iOS 6.0, fixing iOS 6 and Retina 4" iPhone bugs.  Re-create App store GM build.

2012-12-03 - TopXNotes_touch1.2.2 GM Proj - LEG - Create App store GM build.

2012-11-14 - TopXNotes_touch1.2.2d2 Proj - LEG - Finalize how to UI represent sort type and direction.

2012-11-11 - TopXNotes_touch1.2.2d1 Proj - LEG - Reconfigured to use UIBarButtonItems to change between sort types.

2012-11-09  - TopXNotes_touch1.2.2d1 Proj - LEG - Pushed to 4Jim for him to check out - uses UISegmentedControl to change between sort types.

2012-10-17 - TopXNotes_touch1.2.2d0 Proj - LEG - Begin adding sort note list options.

2012-10-17 - TopXNotes_touch1.2.1 GM Proj - LEG - Build golden master version and submit to App Store.

2012-10-15 - TopXNotes touch 1.2.1b5 - LEG - Moved unused images to "images workup".  Moved used images to "images".  Make Ad Hoc version.

2012-10-12 - TopXNotes touch 1.2.1b4 - JHL- Add retroactively added comments on my 2012-08-09  changes 

2012-08-09 - TopXNotes touch 1.2.1b1 - Add syncNextNewNoteID data field to Model.h/m at Dirk's request.  Base code project:  TopXNotes_touch1.2.1 JL Proj.zip.

2012-04-20 - TopXNotes touch 1.2.0 GM JL- Only changes are proper sized new graphics for retina display, and fix to make Illegal Config Error message go away using this technique:
http://stackoverflow.com/questions/8171899/uitableviewcell-style-causes-illegal-configuration-error

2012-04-05 - TopXNotes touch 1.2.0b4.  Completed reengineering sync process so that NoteSync version can be detected on Mac and iOS side.

2012-03-08 - TopXNotes touch 1.2.0b1.  Completed reengineering sync process (mostly on TopXNotes Mac side,) and released for sync testing.

2012-02-15 - TopXNotes touch 1.2.0.  Discovered that I was using the wrong codebase for this version, and that the version should be 1.2 instead of 1.0.4.  Started over using version TopXNotes_touch1.1.0 GM Proj as a base and merged Model.h/m from TopXNotes_touch1.0.4b0 Proj

2012-02-03 - TopXNotes touch 1.0.4.  Begin modifications for "last synchronized date stamp" support.

2010-12-09 - TopXNotes touch 1.0.3.  Begin modifications for Syncing with the Apple Mac App Store version.

2010-06-24 - TopXNotes touch 1.0.2.  Fix problem with personal product key not being validated.

2010-06-15 - TopXNotes touch 1.0.1 goes live in the App Store.

2010-06-03 - Change the way in which Model is archived so that additional information can be saved.  Included code to convert previous version of archived model into version 1.

2010-05-26 - TopXNotes touch 1.0.0 goes live in the App Store and is removed from sale a few days later because the version was inadvertently left as a Beta..

2010-03-13 - Added a MFMailComposeViewController (new with iphoneos 3.0) for sending Email.  This features allows Emailing a note without leaving TopXNotes app.

2010-03-08 - Completed implementation of Backup/Restore process, main UI element is a UIPicker.

2010-03-04 - Moved Settings into a UITableView with 3 entries:  Sync, Backup/Restore, and Note.

2010-02-26 - Pushed TopXNotes 1.6.0d4 and TopXNotes_iPhone 1.0b5 for Sync testing.

2010-01-28 - Begun implementation of Sync in TopXNotes 1.6.0 (Mac) and TopXNotes_iPhone 1.0 following "iSync Mini-Specification".

2009-12-06 - Completed provisioning my iPhone with TopXNotes Ad Hoc Provisioning profile.

2009-07-30 - Improved error handling of sync code by adding a backup of the notepad.

2009-07-27 - Posted a question to Developer Forum Beta about how to make my notepad text view scroll with the background notepaper.  Response from Apple employee bambam was that it was not easy to do and that I should file an enhancement request.

2009-07-27 - Improved notepaper.png to more closely match the line height and created a Sync tab icon.

2009-07-23 - Re-worked prototype syncing process with pseudo TopXNotes application TopXNotes_Sync_Demo because of serious problems encountered when syncing on a real device.  I was stuck on this for about 45 days because I kept trying to get streams to work as Eskimo suggested in reply to my Developer Forums Beta post of 7/3/09.

2009-06-04 - Successfully completed prototype syncing process with pseudo TopXNotes application TopXNotes_Sync_Demo.

2009-05-19 - Successfully integrated WiTap (Apple sample) into TopXNotes so that I can demonstrate exchanging text data between TopXNotes and WiTap on a phone,  Also added a modal view SyncSettingsController to capture the TopXNotes product key which is used for bonjour discovery of peers.

2009-05-16 - Added Sync Demo (cocoa-nut blog sample) code to SyncViewController in order to experiment with syncing between desktop TopXNotes and us.

2009-05-15 - Added WiTap (Apple sample) Bonjour discovery and networking code to project in order to experiment with syncing between desktop TopXNotes and us.

2009-05-10 - Fixed scrolling of NoteView so that insertion point is scrolled into view when the keyboard is present.

2009-05-05 - Renamed project folder from "NotesTopX" (provisional name) to "TopXNotes_iPhone" and project name from "NotesTopX to TopXNotes.

2009-05-03 - Implemented Email tab functionality to mail a note.  Renamed target from NotesTopX to TopXNotes and loaded to a device for the first time.

2009-05-02 - Using Adobe PhotoShop CS4 created tab bar icons notes.png and note pad background notepaper.png.  Use desktop TopXNotes icon for the app's icon icon.png.  Added the Email tab, reusing the NotePadViewController to display the list of notes from which to choose a note to Email.

2009-05-02 - Using Adobe PhotoShop CS4 created tab bar icons notes.png and note pad background notepaper.png.  Use desktop TopXNotes icon for the app's icon icon.png.  Added the Email tab, reusing the NotePadViewController to display the list of notes from which to choose a note to Email.

2009-05-01 - Altered SearchView so that listContent, filteredListContent, and savedContent are now arrays of Note indexes (into Model's  noteList) instead of the Notes themselves.  This solved all my problems with identifying an edited filtered note.

2009-04-28 - Finally was able to get Search tab to work without the UISearchBar being obscured by the UITableView.  Still need to populate table view with list of notes and modify filters to search for text in notes.

2009-04-24 - Fleshed out logic for editing existing notes (Notes tab.)  Begin adding Search tab code using TableSearch and UICatalog samples for ideas.

2009-04-21 - App completed up to the point where it is creating, viewing, and deleting notes.  Still very rough.

2009-04-11 - Completed following GasTracker but found that app not working.  Begin debugging.

2009-04-05 - Use Rich Warren's  2 part article in MacTech, "iPhone Productivity Applications" (Feb and March 2009) as the design pattern.  The article is a detailed account of creating GasTracker, an app for tracking gasoline usage statistics.

2009-04-05 - Create a project from the Tab Bar Application template.
