//
//  NoteFontPickerController.m
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying Fonts and Font Sizes.
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210630 - TopxNotes 1.6
//

#import "Formatter.h"
#import "Constants.h"
#import "Model.h"
#import "NoteFontPickerController.h"


@implementation NoteFontPickerController

@synthesize fontNamesArray, fontSizesArray;
@synthesize pickerView;
@synthesize label;
@synthesize model;
@synthesize saveFontButton;


- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}

- (NSString*)pathToBackupData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kBackupModelFileName];

	return appFile;
}

-(IBAction)saveFontSelection {

	// Reading Defaults 
	NSUserDefaults *defaults;
	NSMutableDictionary *savedSettingsDictionary;
	defaults = [NSUserDefaults standardUserDefaults];

	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil) 
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
		
	// Get Font data from picker
	NSInteger fontNameRow = [pickerView selectedRowInComponent:FONT_NAME_COMPONENT];
	NSInteger fontSizeRow = [pickerView selectedRowInComponent:FONT_SIZE_COMPONENT];
	
	// Update the Font settings in user defaults
	[savedSettingsDictionary setObject:[fontNamesArray objectAtIndex:fontNameRow] forKey:kNoteFontName_Key];
	[savedSettingsDictionary setObject:[fontSizesArray objectAtIndex:fontSizeRow] forKey:kNoteFontSize_Key];
	
	// Save settings
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark  UIPickerViewDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
#pragma unused (pickerView)
    
	return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
#pragma unused (pickerView)
    
	if (component == FONT_NAME_COMPONENT)
		return [fontNamesArray count];
	else
		return [fontSizesArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
#pragma unused (pickerView, component)
    
	if (component == FONT_NAME_COMPONENT)
		return [fontNamesArray objectAtIndex:row];
	else
		return [fontSizesArray objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
#pragma unused (pickerView, component)
    
	if (component == FONT_NAME_COMPONENT) {
		return FONT_NAME_COMPONENT_WIDTH;
	}
	else
		return FONT_SIZE_COMPONENT_WIDTH;
}

-(void)pickerView:(UIPickerView *)inPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
#pragma unused (row, component)
    
	// If the user chooses a new row, update the label accordingly.
	label.text = [NSString stringWithFormat:@"%@ - %@ pt", [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT],
														   [self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_SIZE_COMPONENT] forComponent:FONT_SIZE_COMPONENT]];
	label.font = [UIFont fontWithName:[self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_NAME_COMPONENT] forComponent:FONT_NAME_COMPONENT]
//								 size:[[self pickerView:inPickerView titleForRow:[inPickerView selectedRowInComponent:FONT_SIZE_COMPONENT] forComponent:FONT_SIZE_COMPONENT] intValue]];
                                 size:16.0];                                    //leg20210519 - TopXNotes2
}

// Removed deprecated UIAlertView usage.                                        //leg20210426 - TopXNotes2
//#pragma mark UIAlertView
//
//- (void)alertSimpleAction:(NSString*)alertMessage
//{
//	// open an alert with just an OK button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Backup Notepad" message: alertMessage
//							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//	[alert show];	
//}
//
//- (void)alertOKCancelAction:(NSString*)alertMessage
//{
//	// open a alert with an OK and cancel button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Backup Notepad" message: alertMessage
//							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Proceed", nil];
//	[alert show];
//}
//
//
//#pragma mark - UIAlertViewDelegate
//
//- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{	
//#pragma unused (actionSheet)
//    
//	// use "buttonIndex" to decide your action
//	//
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		//NSLog(@"Cancel");
//	} else {
//		//NSLog(@"OK");
//		//[self doReplaceNotePad];
//	}
//}
//
//#pragma mark UIActionSheet
//
//- (void)dialogOKCancelAction
//{
//	// open a dialog with an OK and cancel button
//	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
//									delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
//	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
//}
//
//#pragma mark - UIActionSheetDelegate
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//#pragma unused (actionSheet)
//    
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		NSLog(@"Proceed");
//		//[self sendEmail];
//	}
//	else
//	{
//		NSLog(@"Cancel");
//	}
//}

@end
