//
//  NotePaperView.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/12/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210630 - TopxNotes 1.6
//

#import "NotePaperView.h"
#import "NotePaperDrawer.h"		//NotePaperView test with Axis - 20110412

@implementation NotePaperView

@synthesize delegate;

@synthesize lineWidth=_lineWidth;

- (void)setLineWidth:(CGFloat)lineWidth {
    _lineWidth = lineWidth;
}

- (CGFloat)lineWidth {
    return _lineWidth;
}

- (void)setup
{
	self.contentMode = UIViewContentModeRedraw;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
		[self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];                                                       //leg20200109 - Catalyst
	[self setup];
}

+ (BOOL)scaleIsValid:(CGFloat)aScale
{
	return ((aScale > 0) && (aScale <= 1));
}

#define DEFAULT_SCALE 0.90

- (CGFloat)scale
{
	return [NotePaperView scaleIsValid:scale] ? scale : DEFAULT_SCALE;
}

- (void)setScale:(CGFloat)newScale
{
	if ([NotePaperView scaleIsValid:newScale]) {
		if (newScale != scale) {
			scale = newScale;
			[self setNeedsDisplay];
		}
	}
}

- (void)pinch:(UIPinchGestureRecognizer *)gesture
{
	if ((gesture.state == UIGestureRecognizerStateChanged) ||
		(gesture.state == UIGestureRecognizerStateEnded)) {
		self.scale *= gesture.scale;
		gesture.scale = 1;
	}
}

- (void)drawCircleAtPoint:(CGPoint)p withRadius:(CGFloat)radius inContext:(CGContextRef)context
{
	UIGraphicsPushContext(context);
	CGContextBeginPath(context);
	CGContextAddArc(context, p.x, p.y, radius, 0, 2*M_PI, YES);
	CGContextStrokePath(context);
	UIGraphicsPopContext();
}

- (void)drawRect:(CGRect)rect
{
//    // Show the ruled notebook paper background on < iOS 7.                     //leg20140212 - 1.2.7
//    if (IS_OS_7_OR_LATER)
//        return;
//
    // Draw lined notebook paper background.
    [NotePaperDrawer drawNotePaperInRect:rect withLineWidths:self.lineWidth];
}


@end
