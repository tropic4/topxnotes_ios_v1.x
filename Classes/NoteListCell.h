//
//  NoteListCell.h
//  NotesTopX
//
//  Created by Lewis Garrett on 5/2/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteListCell : UITableViewCell
{
	NSDictionary	*dataDictionary;
	UILabel			*noteTitleLabel;
	UILabel			*dateLabel;
	UILabel			*infoLabel;
}

@property (nonatomic, strong) NSDictionary *dataDictionary;
@property (nonatomic, strong) UILabel *noteTitleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *infoLabel;

- (id)initWithReuseIdentifier:(NSString *)identifier;                           //leg20140205 - 1.2.7

@end
