//
//  NoteSettingsViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteSettingsInfoViewController.h"

@class NoteFontPickerController;

@class Model;

@interface NoteSettingsViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate> {

	NSMutableDictionary *savedSettingsDictionary;			

	IBOutlet Model			*model;

    UIView *pickerViewContainer;
	
    UIView *noteFontPickerViewContainer;
    NoteFontPickerController *noteFontPickerController;
        
	NSUInteger selectedUnit;

	NoteSettingsInfoViewController	*modalViewController;
	
// Obsolete.                                                                    //leg20210519 - TopXNotes2
//	CGRect originalLocRestoreButton;
//	CGRect landscapeLocRestoreButton;

//	CGRect originalLocFontSampleRect;                                           //leg20140205 - 1.2.7
//	CGRect landscapeLocFontSampleRect;                                          //leg20140205 - 1.2.7
}

@property (nonatomic, strong) Model *model;

@property (nonatomic, strong) IBOutlet UIView *pickerViewContainer;
@property (strong, nonatomic) IBOutlet UILabel *iOS6PointLabel;                 //leg20140205 - 1.2.7
@property (strong, nonatomic) IBOutlet UILabel *iOS7PointLabel;                 //leg20140205 - 1.2.7

@property (nonatomic, strong) IBOutlet NoteFontPickerController *noteFontPickerController;
@property (nonatomic, strong) IBOutlet UIView *noteFontPickerViewContainer;

// Obsolete.                                                                    //leg20210519 - TopXNotes2
//- (void)positionRestoreButton:(UIInterfaceOrientation)toInterfaceOrientation;

- (NSString*)pathToAutoBackup:(NSString*)withFileName;

@end
