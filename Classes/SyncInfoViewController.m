
#import "SyncInfoViewController.h"

@implementation SyncInfoViewController
@synthesize model;
// Obsolete notePaperView.                                                      //leg20210628 - TopXNotes 1.6
//@synthesize notePaperView, instructionsLabel;                                   //leg20140205 - 1.2.7
@synthesize instructionsLabel;                                                  //leg20140205 - 1.2.7

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		//self.title = NSLocalizedString(@"PageSixTitle", @"");
//		self.title = @"About TopXNotes Touch";
        self.title = [NSString stringWithFormat:@"About %@",                    //leg20150924 - 1.5.0
                      [self infoValueForKey:@"CFBundleDisplayName"]];           //leg20150924 - 1.5.0
	}
	
	return self;
}


// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// Automatically invoked after -loadView
// This is the preferred override point for doing additional setup after -initWithNibName:bundle:
//
- (void)viewDidLoad
{

    [super viewDidLoad];
    
	//self.view.backgroundColor = [UIColor whiteColor];	// use the table view background color
	
	appName.text = [self infoValueForKey:@"CFBundleName"];
	//copyright.text = [self infoValueForKey:@"NSHumanReadableCopyright"];
	copyright.text = [self infoValueForKey:@"CFBundleGetInfoString"];

// Obsolete notePaperView.                                                      //leg20210628 - TopXNotes 1.6
//    // The lined notepaper is displayed only if on < iOS 7.                     //leg20140205 - 1.2.7
//    if (!IS_OS_7_OR_LATER) {
//        // Adjust lined notebook paper background.
//        self.notePaperView.frame = CGRectMake(self.notePaperView.frame.origin.x,
//                                              self.notePaperView.frame.origin.y - 24,
//                                              self.notePaperView.frame.size.width,
//                                              self.notePaperView.frame.size.height);
//    }
//
//    // Insure lineWidth is not 0.
//    self.notePaperView.lineWidth = instructionsLabel.font.lineHeight;
}

- (void)viewWillAppear:(BOOL)animated {                                         //leg20140205 - 1.2.7

    [super viewWillAppear:animated];
    
// Obsolete notePaperView.                                                      //leg20210628 - TopXNotes 1.6
//    // The lined notepaper is displayed only if on < iOS 7.
//    if (!IS_OS_7_OR_LATER) {
//        self.notePaperView.lineWidth = instructionsLabel.font.lineHeight;
//        [self.view setNeedsDisplay];
//        [self.notePaperView setNeedsDisplay];
//    }
//
//    // Maybe make showing notebook paper background an option in iOS 7.         //leg20140212 - 1.2.7
//    if (YES) {
//        self.notePaperView.hidden = YES;
//        self.view.backgroundColor = [UIColor whiteColor];
//    }
}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)

    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)

    [super viewDidAppear:animated];
    
	// do something here as our view re-appears
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                         //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
