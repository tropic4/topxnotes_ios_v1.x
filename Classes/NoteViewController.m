//
//  NoteViewController.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Iota. All rights reserved.
//
// Convert from .xib to storyboard segues.                                      //leg20210626 - TopxNotes 1.6
//

#import "TopXNotesAppDelegate.h"                                                //leg20210705 - TopxNotes 1.6
#import "BackupSettingsViewController.h"                                        //leg20140405 - 1.5.0
#import "NoteViewController.h"
#import "Constants.h"
#import "Formatter.h"
#import "Model.h"
#import "Note.h"
#import "NSMutableString+EmailEncodingExtensions.h"
#import "HoverView.h"								//leg20110421 - 1.0.4

NSString *Show_HoverView = @"SHOW";					//leg20110421 - 1.0.4

// Define private methods
@interface NoteViewController()
@end

@implementation NoteViewController

@synthesize notePaperView, slider;					//leg20110419 - 1.0.4
@synthesize fontSliderValue;						//leg20110419 - 1.0.4
@synthesize model;									//leg20110419 - 1.0.4
@synthesize noteFontName, noteFontSize;				//leg20110420 - 1.0.4

@synthesize hoverView, emailButton;					//leg20110421 - 1.0.4
@synthesize tapRecognizer, swipeLeftRecognizer, longPressRecognizer;			//leg20110421 - 1.0.4
@synthesize toolsHUDButton;														//leg20110531 - 1.0.4
@synthesize noteCreationDate, barButtonItem;                                    //leg20140212 - 1.2.7
@synthesize noteIndex;                                                          //leg20210626 - TopxNotes 1.6

- (void)updateUI                                                                //leg20110419 - 1.0.4
{
	self.slider.value = self.fontSliderValue;

//	// Validate fontSize
//	float fontSize = 0.0;
//	fontSize = [self fontSizeForNotePaperView:nil];
//	if (fontSize < 0.0) fontSize = 0.0;
//	if (fontSize > 100.0) fontSize = 100.0; //leg20121011 - QUESTION: Why is local variable fontSize not referenced?
	noteText.font = [UIFont fontWithName:noteFontName size:[noteFontSize intValue]];
	

	// Get width to make note paper lines.
    // Test for iOS 4 or greater fails and causes hang when NoteView is         //leg20210614 - TopXNotes 1.6
    //  is presented.  Commented-out test since Deployment target is now 9.2.
//	NSComparisonResult compareResult = [@"4.0" compare:[[UIDevice currentDevice] systemVersion]];
//	if (compareResult == NSOrderedSame || compareResult == NSOrderedAscending)
		self.notePaperView.lineWidth = noteText.font.lineHeight;	// @Property available beginning in iOS 4.0
//	else
//		self.notePaperView.lineWidth = noteText.font.leading;

	[self.view setNeedsDisplay];
	[self.notePaperView setNeedsDisplay];
}

- (void)setFontSliderValue:(int)newFontSliderValue
{
    if (newFontSliderValue < 0) newFontSliderValue = 0;
	if (newFontSliderValue > 100) newFontSliderValue = 100;
	fontSliderValue = newFontSliderValue;
	NSMutableString *fontSizeString = (NSMutableString*)[NSString               //leg20140212 - 1.2.7
                                        stringWithFormat:@"%d",
                                        (int)[self fontSizeForNotePaperView:nil]];
	self.noteFontSize = fontSizeString;	
	[self updateUI];
}

- (IBAction)fontSliderValueChanged:(UISlider *)sender
{
	self.fontSliderValue = sender.value;
}

//- (float)fontSizeForNotePaperView:(NotePaperView *)requestor
- (float)fontSizeForNotePaperView:(id)requestor
{
#pragma unused (requestor)

	float fontSize = 0;
	//if (requestor == self.notePaperView) {
		if (self.fontSliderValue <= 10) {
			fontSize = 8.0;
		} else if (self.fontSliderValue <= 20) {
			fontSize = 10.0;
		} else if (self.fontSliderValue <= 30) {
			fontSize = 12.0;
		} else if (self.fontSliderValue <= 40) {
			fontSize = 14.0;
		} else if (self.fontSliderValue <= 50) {
			fontSize = 16.0;
		} else if (self.fontSliderValue <= 60) {
			fontSize = 18.0;
		} else if (self.fontSliderValue <= 70) {
			fontSize = 20.0;
		} else if (self.fontSliderValue <= 80) {
			fontSize = 22.0;
		} else if (self.fontSliderValue <= 90) {
			fontSize = 24.0;
		} else if (self.fontSliderValue <= 100) {
			fontSize = 26.0;
		}
	//}
	return fontSize;
}

- (void)updateFontSizeSlider
{
	if ([noteFontSize intValue] == 8) {
		self.fontSliderValue = 10;
	} else if ([noteFontSize intValue] == 10) {
		self.fontSliderValue = 20;
	} else if ([noteFontSize intValue] == 12) {
		self.fontSliderValue = 30;
	} else if ([noteFontSize intValue] == 14) {
		self.fontSliderValue = 40;
	} else if ([noteFontSize intValue] == 16) {
		self.fontSliderValue = 50;
	} else if ([noteFontSize intValue] == 18) {
		self.fontSliderValue = 60;
	} else if ([noteFontSize intValue] == 20) {
		self.fontSliderValue = 70;
	} else if ([noteFontSize intValue] == 22) {
		self.fontSliderValue = 80;
	} else if ([noteFontSize intValue] == 24) {
		self.fontSliderValue = 90;
	} else if ([noteFontSize intValue] == 26) {
		self.fontSliderValue = 100;
	} else {
		self.fontSliderValue = 100;
	}
}

- (void)viewDidLoadNotePaperView                                                //leg20110419 - 1.0.4
{
	[self updateFontSizeSlider];

	self.notePaperView.delegate = self;
	UIGestureRecognizer *pinchgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self.notePaperView action:@selector(pinch:)];
	[self.notePaperView addGestureRecognizer:pinchgr];
	[self updateUI];
}


#pragma mark Actions

-(IBAction)toggleToolsHUD:(UIBarButtonItem *)sender {                           //leg20140205 - 1.2.7
    
    // Remove gesture recognizer while heads-up display is active
    [self.view removeGestureRecognizer:swipeLeftRecognizer];
    
    // Save the bar button so that it can be re-enabled once timer has expired.
    self.barButtonItem = sender;
	self.barButtonItem.enabled = NO;
    
	[[NSNotificationCenter defaultCenter] postNotificationName:Show_HoverView object:nil];
}

//-(IBAction)showBackupRestore:(UIBarButtonItem *)sender {                        //leg20140405 - 1.5.0
//#pragma unused (sender)
//
//    BackupSettingsViewController *backupSettingsViewController = [[BackupSettingsViewController alloc] initWithNibName:@"BackupSettingsView" bundle:nil];
//    backupSettingsViewController.model = self.model;
//    [self.navigationController pushViewController:backupSettingsViewController animated:YES];
//}
-(IBAction)showBackupRestore:(UIBarButtonItem *)sender {                        //leg20210701 - TopxNotes 1.6
#pragma unused (sender)
    
    // This is now handled by segue @"Show_Backup_Restore_Segue".               //leg20210701 - TopxNotes 1.6
    [self performSegueWithIdentifier:@"Show_Backup_Restore_Segue" sender: self];
}

-(IBAction)toggleNotepaperBG:(UIBarButtonItem *)sender {                        //leg20140216 - 1.2.7
#pragma unused (sender)

    // Show/Hide notebook paper background.
    if (showNotepaper) {
        self.notePaperView.hidden = YES;
        showNotepaper = NO;
    } else {
        self.notePaperView.hidden = NO;
        showNotepaper = YES;
    }
    
	// Save show/hide notepaper background state
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];

	// Set current state of show/hide notepaper.
    [savedSettingsDictionary setObject:[NSNumber numberWithBool:showNotepaper] forKey:kShowNotepaper_Key];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

-(IBAction)done {
	// If Emailing a note we must eat keyboard notifications so as to not		//leg20100701 - 1.0.2
	//	crash when we come out of Mail Composer.
	if (emailInProgress) {
		return;
	}
	
	NSString* titleString;
	NSDate* date = [NSDate date];
	noteIsDirty = NO;				// Mark note no longer dirty.				//leg20100701 - 1.0.2

	// If note was unchanged pop view controller, else update the model with changed note
	if ([doneButton.title isEqualToString:@"Edit"]) {							//leg20140205 - 1.2.7

        self.navigationController.toolbarHidden=YES;                            //leg20140222 - 1.2.7

		// Enable Done button
		doneButton.enabled = YES;
		doneButton.style = UIBarButtonItemStyleDone;							
		doneButton.title = @"Done";											

		// Move function of changing text from -textViewDidChangeSelection to here.			//leg20110420 - 1.0.4
		//[self.navigationController popViewControllerAnimated:YES];
		noteText.editable = YES;
		noteText.textColor = [UIColor brownColor];

        // Scroll the insertion point into view.                                //leg20140222 - 1.2.7
//        if (IS_OS_7_OR_LATER)
//            [noteText scrollRangeToVisible:noteText.selectedRange];

    } else if ([doneButton.title isEqualToString:@"Save"]) {                    //leg20140205 - 1.2.7

        self.navigationController.toolbarHidden=NO;                             //leg20140222 - 1.2.7

		// Create note title from 1st kNoteTitleMaxLength characters of text
		if ([noteText.text length] < kNoteTitleMaxLength)		
			titleString = noteText.text;
		else
			titleString = [noteText.text substringToIndex:(NSUInteger)kNoteTitleMaxLength];

		// Make title end at first newline character
		NSRange theRange = [titleString rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]];
		if (theRange.location != NSNotFound)
			titleString = [titleString substringToIndex:theRange.location];

//		Note *note = [[Note alloc] initWithTitle: titleString
//									  text: noteText.text
//									withID: noteID
//									onDate: date];
        // Use a new initWithTitle method which allows specification of the     //leg20140212 - 1.2.7
        //  creation date.  This solves the problem of the creation date being
        //  updated every time a note is edited.
        Note *note = [[Note alloc] initWithTitle: titleString                   //leg20140212 - 1.2.7
                                            text: noteText.text
                                          withID: noteID
                                          onDate: date
                                      createDate:self.noteCreationDate];
		
		// Mark note as eligible to be synced
		note.syncFlag = [NSNumber numberWithBool:YES];

		// Mark note needs to be synced
		note.needsSyncFlag = [NSNumber numberWithBool:YES];
		
		// Update the data model of notes
		[model replaceNoteAtIndex:noteIndex withNote:note];                     //leg20130303 - 1.2.3
		
		// Update the note index since the note is now at the top of the list	//leg20100628 - 1.0.2
		noteIndex = 0;
		
		// Enable Done button 
		doneButton.enabled = YES;
		doneButton.style = UIBarButtonItemStylePlain;							//leg20110420 - 1.0.4
		doneButton.title = @"Edit";												//leg20110428 - 1.0.4

		// Restore the UITextView to not editing state.							//leg20110420 - 1.0.4
		noteText.editable = NO;
		noteText.textColor = [UIColor blackColor];


        // Scroll the insertion point into view.                                //leg20140222 - 1.2.7
        if (IS_OS_7_OR_LATER)
            [noteText scrollRangeToVisible:noteText.selectedRange];

	} else {
		// "Done"  note unchanged												//leg20110517 - 1.0.4
        self.navigationController.toolbarHidden=NO;                             //leg20140222 - 1.2.7
		
		// Enable Done button													
		doneButton.enabled = YES;
		doneButton.style = UIBarButtonItemStylePlain;							
		doneButton.title = @"Edit";												

		// Restore the UITextView to not editing state.							
		noteText.editable = NO;
		noteText.textColor = [UIColor blackColor];
	}
}

// fetch objects from our bundle based on keys in our Info.plist                //leg20150924 - 1.5.0
- (id)infoValueForKey:(NSString*)key
{
    if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
        return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

- (id)initWithNibName:(NSString *)nibNameOrNil noteIndex:(int)index {
	noteIndex = index;
	
	// Insure Email In Progress flag is initially off.							//leg20100701 - 1.0.2
	emailInProgress = NO;
	
	// Get font information.													//leg20110420 - 1.0.4
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil) 
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];

	// Get Font names and sizes															
	noteFontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
	if (noteFontName == nil) {
		noteFontName = (NSMutableString*)kNoteTextFont;
		[savedSettingsDictionary setObject:noteFontName forKey:kNoteFontName_Key];
	}
	noteFontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
	if (noteFontSize == nil) {
		noteFontSize = (NSMutableString*)[NSString stringWithFormat:@"%d", kNoteTextFontSize];
		[savedSettingsDictionary setObject:noteFontSize forKey:kNoteFontSize_Key];
	}

	// Save settings
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
	
    if (self = [super initWithNibName:nibNameOrNil bundle:nil]) {
	}
    return self;
}

// In response to a swipe gesture, show the HUD.
 - (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer {
	// Remove gesture recognizer while heads-up display is active
	[self.view removeGestureRecognizer:recognizer];
	[[NSNotificationCenter defaultCenter] postNotificationName:Show_HoverView object:nil];
}

// Pass information to view controller segued to.                               //leg20210701 - TopxNotes 1.6
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Show_Backup_Restore_Segue"] ) {
        // Pass model to EncryptionSettingsViewController view.
        BackupSettingsViewController *controller = (BackupSettingsViewController *)segue.destinationViewController;
        controller.model = self.model;
    }
}

// Segue back to NotePad.                                                       //leg20210705 - TopxNotes 1.6
- (void)goBackToNotePad {
    [self performSegueWithIdentifier:@"unwind_to_notepad_segue" sender: self];
}

// Implement viewDidLoad to do additional setup after loading the view.
- (void)viewDidLoad {
    [super viewDidLoad];

//    // Fix -viewWillAppear not occurring for NoteSettingsViewController.        //leg20210623 - TopXNotes 1.6
//    self.modalPresentationStyle = UIModalPresentationFullScreen;
    
    // Gesture recognizers available beginning in iOS 3.2.                      //leg20210627 - TopxNotes 1.6
    
    // Since we can have a heads-up display we don't need the Email button on the text view.
    //emailButton.hidden = YES;

    // Create a swipe gesture recognizer to recognize left or right swipes.
    //  Keep a reference to the recognizer so that it can be added to and removed from the view.
    swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
    swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft+UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipeLeftRecognizer];
    swipeLeftRecognizer.delegate = self;

	// Since we now bring up the HUD on all devices we do not need the separate email button.
	emailButton.hidden = YES;

	// Prepare our note paper background.										//leg20110419 - 1.0.4
	[self viewDidLoadNotePaperView];											

	// Prepare our heads-up display.											//leg20110419 - 1.0.4
	
	// Determine the size of HoverView
//	CGRect frame = hoverView.frame;
//	frame.origin.x = round((self.view.frame.size.width - frame.size.width) / 2.0);
//	frame.origin.y = self.view.frame.size.height - 100;
//	hoverView.frame = frame;
	[self.view addSubview:self.hoverView];                                      //leg20140212 - 1.2.7
	
	// Called by MainView, when the user swipes left or right.
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showViewNotif:) name:Show_HoverView object:nil];

    // Either adding a new note or editing an existing note.                    //leg20130110 - 1.2.2
    if (noteIndex == -1) {
        // Adding a new note.
        noteText.text = @"";

        syncFlag = [NSNumber numberWithBool:YES];
        needsSyncFlag = [NSNumber numberWithBool:YES];
        self.noteCreationDate = [NSDate date];                                  //leg20140212 - 1.2.7
        
        // this will appear as the title in the navigation bar
        self.title = @"";
    } else {
        // Existing note.
        Note *note = [model getNoteForIndex:noteIndex];
        
        // Refresh from source each time.			
        noteText.text = note.noteText;
        
        syncFlag = note.syncFlag;
        noteID = note.noteID;
        needsSyncFlag = note.needsSyncFlag;
        self.noteCreationDate = note.createDate;                                //leg20140212 - 1.2.7
        
        // this will appear as the title in the navigation bar
        self.title = note.title;
    }

    // Build a toolbar.                                                         //leg20140205 - 1.2.7
    UIBarButtonItem * showHUDButtonItem = [[UIBarButtonItem alloc]
                                           initWithImage:[UIImage
                                                          imageNamed:@"fonts.png"]
                                           style:UIBarButtonItemStylePlain
                                           target:self
                                           action:@selector(toggleToolsHUD:)];
    
    UIBarButtonItem * toggleNotepaperBGButtonItem = [[UIBarButtonItem alloc]
                                                initWithImage:[UIImage
                                                imageNamed:@"notepaperBG.png"]
                                                style:UIBarButtonItemStylePlain
                                                target:self
                                                action:@selector(toggleNotepaperBG:)];

    UIBarButtonItem * bkupRestoreButtonItem = [[UIBarButtonItem alloc]
                                               initWithImage:[UIImage
                                                imageNamed:@"data_backup-32"]
                                               style:UIBarButtonItemStylePlain
                                               target:self
                                               action:@selector(showBackupRestore:)]; //leg20140405 - 1.5.0

    UIBarButtonItem * actionButtonItem = [[UIBarButtonItem alloc]
                                            initWithBarButtonSystemItem:UIBarButtonSystemItemAction
                                            target:self
                                            action:@selector(emailNote)];

    // Spacer button
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    self.navigationController.toolbarHidden=NO;
    self.navigationController.toolbar.barStyle = UIBarStyleBlack;
    self.navigationController.toolbar.translucent = YES;
    self.toolbarItems = [NSArray arrayWithObjects:toggleNotepaperBGButtonItem, flexiableItem, showHUDButtonItem, bkupRestoreButtonItem, flexiableItem, actionButtonItem, nil];
    
//    // Force viewWillAppear on iOS 13 or greater so NoteView inited properly.   //leg20210616 - TopXNotes 1.6
//    if (@available(iOS 13, *)) {
//        [self viewWillAppear:YES];
//    }
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];
    
    // Must return to notepad if notepad has been deleted or replaced by a      //leg20210705 - TopXNotes 1.6
    //  backup.  Necessary because the current note may have disappeared
    //  as a result of the "replace" operation, and cause a crash.  Note that
    //  the current note and any changes to it are lost.
    TopXNotesAppDelegate *appDelegate =
        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate.notepadWasReplaced) {
        appDelegate.notepadWasReplaced = NO;
        [self goBackToNotePad];                                                 //leg20210705 - TopxNotes 1.6
        return;
    }

    // add our custom button as the nav bar's custom right view
	doneButton = [[UIBarButtonItem alloc]
                   initWithTitle:@"Edit"                                        //leg20110428 - 1.0.4
                   style:UIBarButtonItemStyleBordered
                   target:self
                   action:@selector(action:)];
	// Enable "Done" button.
	doneButton.enabled = YES;
	doneButton.style = UIBarButtonItemStylePlain;								//leg20110420 - 1.0.4
	self.navigationItem.rightBarButtonItem = doneButton;
    
	dateLabel.text = dateString;

	// Get text characteristics from defaults dictionary.						//leg20110420 - 1.0.4
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
	noteFontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
	noteFontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];

    // Get show/hide notepaper state.                                           //leg20140222 - 1.2.7
	showNotepaper = [[savedSettingsDictionary objectForKey:kShowNotepaper_Key] boolValue];

    // Restore show/hide notebook paper background state.
    if (showNotepaper) {
        self.notePaperView.hidden = NO;
    } else {
        self.notePaperView.hidden = YES;
    }
    
    
    // Set text attributes.
    noteText.textColor = [UIColor blackColor];
    noteText.font = [UIFont fontWithName:noteFontName size:[noteFontSize intValue]];
	[self updateFontSizeSlider];
	[self updateUI];
    
    // Either adding a new note or editing an existing note.                    //leg20130110 - 1.2.2
    if (noteIndex == -1) {
        noteIndex = 0;      // Reset noteIndex to first note in list.
        
        Note* note = [[Note alloc] initWithTitle: @""
                                            text: @""
                                          withID: [NSNumber numberWithUnsignedLong:0]
                                          onDate: [NSDate date]];
        
        // Save noteID for when added note is saved.                            //leg20130301 - 1.2.3
        noteID = note.noteID;
        
        // Mark note as eligible to be synced
        note.syncFlag = [NSNumber numberWithBool:YES];
        
        // Mark note needs to be synced
        note.needsSyncFlag = [NSNumber numberWithBool:YES];
        
		// Add note to front of list, so it will have an index of 0.
		[model addNote:note];
        
		// Enable Done button
		doneButton.enabled = YES;
		doneButton.style = UIBarButtonItemStyleDone;
		doneButton.title = @"Done";
        
        // Make text editable.
		noteText.editable = YES;
		noteText.textColor = [UIColor brownColor];
        noteText.delegate = self;
        [noteText becomeFirstResponder];
//        [self registerForKeyboardNotifications];
        
    } else {
        Note *note = [model getNoteForIndex:noteIndex];
        noteText.text = note.noteText;
    }
	
	[self registerForKeyboardNotifications];
	noteIsDirty = NO;
}

// Force viewWillDisappear on iOS 13 or greater so NoteView cleaned-up.         //leg20210617 - TopXNotes 1.6
- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];

//    if (@available(iOS 13, *)) {
//        [self viewWillDisappear:YES];
//    }
}

- (void)viewWillDisappear:(BOOL)animated
{
#pragma unused (animated)
    
    [super viewWillDisappear:animated];
    
    // Stop any HUD timer that might be counting down.                          //leg20140205 - 1.2.7
    [self killTimer];
//	[self showHoverView:(self.hoverView.alpha != 1.0)]; // Show if not visible. //leg20140212 - 1.2.7
	[self showHoverView:NO];    // Hide HUD.                                    //leg20140222 - 1.2.7
    
	// Make sure note gets updated if user switches to another view.
	if (noteIsDirty)
		[self done];
}

- (void)killTimer                                                               //leg20140205 - 1.2.7
{
	// reset the HUD timer
	[myTimer invalidate];
	myTimer = nil;

    self.barButtonItem.enabled = YES;                                           //leg20140205 - 1.2.7
}

- (void)handleTap:(UITapGestureRecognizer *)sender                              //leg20140212 - 1.2.7
{
     // calculate whether touchPoint was within hoverview
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint touchPoint = [sender locationInView:self.view];
        if (!CGRectContainsPoint (hoverView.frame, touchPoint)) {
        
            // Touch point was in the text view, a signal to hide HUD.
            [self killTimer];
            [self showHoverView:NO];
        }
    }
}

- (void)showHoverView:(BOOL)show                                                //leg20110421 - 1.0.4
{
    // Stop any HUD timer that might be counting down.                          //leg20140205 - 1.2.7

	// fade animate the view out of view by affecting its alpha
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.40];

	if (show)
	{
        
		// Create a tap gesture recognizer to recognize single taps.            //leg20140212 - 1.2.7
		//  Keep a reference to the recognizer so that it can be added to
        //  and removed from the view.
		tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
		tapRecognizer.numberOfTapsRequired = 1;
		[self.view addGestureRecognizer:tapRecognizer];
		tapRecognizer.delegate = self;

        self.barButtonItem.enabled = NO;                                        //leg20140212 - 1.2.7
		// as we start the fade effect, start the timeout timer for automatically hiding HoverView
		self.hoverView.alpha = 1.0;                                             //leg20140212 - 1.2.7
		myTimer = [NSTimer timerWithTimeInterval:15.0 target:self selector:@selector(timerFired:) userInfo:nil repeats:NO];
		[[NSRunLoop currentRunLoop] addTimer:myTimer forMode:NSDefaultRunLoopMode];
	}
	else
	{
        // Remove gesture recognizer while heads-up display is inactive.        //leg20140212 - 1.2.7
        [self.view removeGestureRecognizer:tapRecognizer];

		self.hoverView.alpha = 0.0;                                             //leg20140212 - 1.2.7
        self.barButtonItem.enabled = YES;                                       //leg20140212 - 1.2.7
	}
	
	[UIView commitAnimations];
}

- (void)timerFired:(NSTimer *)timer
{
#pragma unused (timer)

	// time has passed, hide the HoverView
	[self showHoverView: NO];

	// Check to see gesture recognizer available on this device.				
	NSComparisonResult compareResult = [@"3.2" compare:[[UIDevice currentDevice] systemVersion]];	
	if (compareResult == NSOrderedSame || compareResult == NSOrderedAscending) {
		// Gesture recognizers available beginning in iOS 3.2.
		
		// Restore gesture recognizer
		[self.view addGestureRecognizer:swipeLeftRecognizer];
	}
}

- (void)showViewNotif:(NSNotification *)aNotification
{
#pragma unused (aNotification)
	[self showHoverView:(self.hoverView.alpha != 1.0)]; // Show if not visible. //leg20140212 - 1.2.7
}


- (void)action:(id)sender
{
#pragma unused (sender)

	// the custom right view button was clicked, handle it here
	//
	// Make the keyboard go away
	[noteText resignFirstResponder];
	
	//•leg 06/09/09 - scrolling experiment - scroll down to 50th character
	//[noteText scrollRangeToVisible:NSMakeRange(50, 1)];

	[self done];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)

    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
NSLog(@"NoteViewController didReceiveMemoryWarning");
}


- (void)dealloc {
	//[savedSettingsDictionary release];
	//[dateString release];
	
	
	                                              //leg20110421 - 1.0.4
	
	// no longer interested in Show_HoverView notifs
	[[NSNotificationCenter defaultCenter] removeObserver:self name:Show_HoverView object:nil];
	
}

#pragma mark UITextViewDelegate

//- (void)textViewDidChange:(UITextView *)textView {
//    CGRect line = [textView caretRectForPosition:
//                   textView.selectedTextRange.start];
//    CGFloat overflow = line.origin.y + line.size.height
//    - ( textView.contentOffset.y + textView.bounds.size.height
//       - textView.contentInset.bottom - textView.contentInset.top );
//    if ( overflow > 0 ) {
//        // We are at the bottom of the visible text and introduced a line feed, scroll down (iOS 7 does not do it)
//        // Scroll caret to visible area
//        CGPoint offset = textView.contentOffset;
//        offset.y += overflow + 7; // leave 7 pixels margin
//                                  // Cannot animate with setContentOffset:animated: or caret will not appear
//        [UIView animateWithDuration:.2 animations:^{
//            [textView setContentOffset:offset];
//        }];
//    }
//}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
#pragma unused (textView, range, text)

	// Since the text changed, enable the Save button.
    doneButton.title = @"Save";
	doneButton.style = UIBarButtonItemStyleDone;								//leg20110420 - 1.0.4
    //[doneButton setTitle:@"Save" forState:UIControlStateNormal];
    //[doneButton setTitle:@"Save" forState:UIControlStateHighlighted];
	doneButton.enabled = YES;
	noteIsDirty = YES;
	return YES;
}

// Detect changed text in case of dictation.                                    //leg20210630 - TopxNotes 1.6
- (void)textViewDidChange:(UITextView *)textView {
#pragma unused (textView)
    // Since the text changed, enable the Save button.
    doneButton.title = @"Save";
    doneButton.style = UIBarButtonItemStyleDone;                                //leg20110420 - 1.0.4
    doneButton.enabled = YES;
    noteIsDirty = YES;
}

//- (void)textViewDidBeginEditing:(UITextView *)textView {
//
//	NSRange selectedRange = noteText.selectedRange;
//	selectedRange.location = 30;
//	noteText.selectedRange = selectedRange;
//	noteText.editable = YES;
//}

//- (void)textViewDidChangeSelection:(UITextView *)textView {
//#pragma unused (textView)
//
//// Move function of changing text to editable the Edit button member.			//leg20110420 - 1.0.4
////	noteText.editable = YES;
////    noteText.textColor = [UIColor brownColor];
//}

//- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
//
//	NSRange selectedRange = noteText.selectedRange;
//	selectedRange.location = 30;
//	noteText.selectedRange = selectedRange;
//	return YES;
//}

#pragma mark Scrolling

- (void)registerForKeyboardNotifications
{
//    [[NSNotificationCenter defaultCenter] addObserver:self
//            selector:@selector(keyboardWasShown:)
//            name:UIKeyboardDidShowNotification object:nil];
// 
//    [[NSNotificationCenter defaultCenter] addObserver:self
//            selector:@selector(keyboardWasHidden:)
//            name:UIKeyboardDidHideNotification object:nil];

    // observe keyboard hide and show notifications to resize the text view appropriately   //leg20140222 - 1.2.7
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}
 
#pragma mark - Responding to keyboard events

CGRect frameRect;                                                               //leg20140222 - 1.2.7
- (void)keyboardWillShow:(NSNotification *)notification {                       //leg20140222 - 1.2.7
    
    frameRect = noteText.frame;
    /*
     Reduce the size of the text view so that it's not obscured by the keyboard.
     Animate the resize so that it's in sync with the appearance of the keyboard.
     */
    
    NSDictionary *userInfo = [notification userInfo];
    
    // Get the origin of the keyboard when it's displayed.
    NSValue *aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    // Get the top of the keyboard as the y coordinate of its origin in self's view's
    // coordinate system. The bottom of the text view's frame should align with the top
    // of the keyboard's final position.
    //
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
//    newTextViewFrame.origin.y -= 10;    //legx
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    // Get the duration of the animation.
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    // Animate the resize of the text view's frame in sync with the keyboard's appearance.
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
//    self.textView.frame = newTextViewFrame;
    noteText.frame = newTextViewFrame;
    
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {                       //leg20140222 - 1.2.7
    
    NSDictionary *userInfo = [notification userInfo];
    
    /*
     Restore the size of the text view (fill self's view).
     Animate the resize so that it's in sync with the disappearance of the keyboard.
     */
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
//    self.textView.frame = self.view.bounds;
    noteText.frame = self.view.bounds;
//    noteText.frame = frameRect;
    
    [UIView commitAnimations];
}

//// Called when the UIKeyboardDidShowNotification is sent.
//- (void)keyboardWasShown:(NSNotification*)aNotification
//{
//	// If Emailing a note is in progress we must eat keyboard notifications		//leg20100701 - 1.0.2
//	//	so as to not crash when we come out of Mail Composer.
//	if (emailInProgress) {
//		return;
//	}
//
//    if (keyboardShown)
//        return;
// 
//    NSDictionary* info = [aNotification userInfo];
//
//    // Get the size of the keyboard.
//    NSValue* aValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
////    NSValue* aValue = [info objectForKey:UIKeyboardFrameBeginUserInfoKey]; // in landscape text move up out of view
//    CGSize keyboardSize = [aValue CGRectValue].size;
// 
//    // Resize the scroll view (which is the root view of the window)
//    CGRect viewFrame = [noteText frame];
//    viewFrame.size.height -= keyboardSize.height-self.navigationController.navigationBar.frame.size.height;
//	noteText.frame = viewFrame;
// 
//    // Scroll the insertion point into view.
//	[noteText scrollRangeToVisible:noteText.selectedRange];
//    keyboardShown = YES;
//}
// 
// 
//// Called when the UIKeyboardDidHideNotification is sent
//- (void)keyboardWasHidden:(NSNotification*)aNotification
//{
//	// If Emailing a note is in progress we must eat keyboard notifications		//leg20100701 - 1.0.2
//	//	so as to not crash when we come out of Mail Composer.
//	if (emailInProgress) {
//		return;
//	}
//
//    NSDictionary* info = [aNotification userInfo];
// 
//    // Get the size of the keyboard.
//    NSValue* aValue = [info objectForKey:UIKeyboardBoundsUserInfoKey];
////    NSValue* aValue = [info objectForKey:UIKeyboardFrameEndUserInfoKey]; // in landscape text move up out of view
//    CGSize keyboardSize = [aValue CGRectValue].size;
// 
//    // Reset the height of the scroll view to its original value
//    CGRect viewFrame = [noteText frame];
//    viewFrame.size.height += keyboardSize.height-self.navigationController.navigationBar.frame.size.height;;
//    noteText.frame = viewFrame;
// 
//    keyboardShown = NO;
//}

#pragma mark Emailing

// Respond to Email UIButton
- (void)emailNote
{
	Note *note = [model getNoteForIndex:noteIndex];
	
	// open a dialog with an OK and cancel button
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat: @"Email note \"%@\"?", note.title]
			delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;

	// Show the dialog coming out of the tab bar		
	UITabBar *tabBar = self.tabBarController.tabBar;
	[actionSheet showFromTabBar:tabBar];
}


// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
	Note *note = [model getNoteForIndex:noteIndex];

    // Assemble the Email
    //	NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
    NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];    //leg20150924 - 1.5.0
    NSString *subjectPrefix = [NSString stringWithFormat:@"Re: %@ %@ --> \"",   //leg20150924 - 1.5.0
                               [self infoValueForKey:@"CFBundleDisplayName"],   //leg20150924 - 1.5.0
                               versionInfo];
	NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	NSMutableString *body = [NSMutableString stringWithString:note.noteText];

	// encode the strings for email
	[subject encodeForEmail];

	[body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];	// Make sure CRs are recognized
	[body encodeForEmail];

	NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:?subject=%@&body=%@", subject, body];
	NSURL *encodedURL = [NSURL URLWithString:emailMsg];
		 
	// Send the assembled message to Mail!
	[[UIApplication sharedApplication] openURL:encodedURL];
}


- (void)sendEmail
{

	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

-(void)displayComposerSheet 
{
	// Insure Email In Progress flag is turned ON.								//leg20100701 - 1.0.2
	emailInProgress = YES;
	
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
       
	Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email - Don't set any recipients, let user do during composition
    //	NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
    NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];    //leg20150924 - 1.5.0
    NSString *subjectPrefix = [NSString stringWithFormat:@"Re: %@ %@ --> \"",   //leg20150924 - 1.5.0
                               [self infoValueForKey:@"CFBundleDisplayName"],   //leg20150924 - 1.5.0
                               versionInfo];
	NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	
	// Make sure CRs are recognized		//•leg - 05/26/10
	//[body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];	
	//[picker setSubject:subject];
	//[picker setMessageBody:body isHTML:YES];		// Say it is HTML so CRs are recognized

	// Change carriage returns to line feeds so they willl be recognized.		//leg20100701 - 1.0.2
	[body replaceOccurrencesOfString:@"\r" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];	
	[picker setSubject:subject];
	[picker setMessageBody:body isHTML:NO];
	
	// Attach an image to the email
	//	Note:  tried this with several pngs, including "rainy" from MailComposer sample.  Image looks
	//			good in composer but in received mail in Mail on Mac, it is always unreadable--Preview
	//			just shows a checkerboard.  GraphicConverter and Adobe Photoshop both say its corrupt.
	//			Maybe has something to do with mime type?  MailComposer has same problem.  Image looks
	//			good if message is received by Mail on the iPhone.
	//NSString *path = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
    //NSData *myData = [NSData dataWithContentsOfFile:path];
	//[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"icon"];

	// Present the composer
    [self presentViewController:picker animated:YES completion:nil];            //leg20140205 - 1.2.7
	
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{
#pragma unused (controller, error)

	emailInProgress = NO;
	NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }
    
        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7

		[self alertEmailStatus:resultMessage];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];	
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	[alert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	
#pragma unused (actionSheet)

	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Cancel");
	} else {
		//NSLog(@"OK");
		[self sendEmail];
	}
}

#pragma mark UIActionSheet

- (void)dialogOKCancelAction
{
	// open a dialog with an OK and cancel button
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
									delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Proceed");
		[self sendEmail];
	}
	else
	{
		//NSLog(@"Cancel");
	}
	
	//[myTableView deselectRowAtIndexPath:[myTableView indexPathForSelectedRow] animated:NO];
}


@end


