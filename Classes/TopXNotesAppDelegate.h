//
//  TopXNotesAppDelegate.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/5/09.
//  Copyright Iota 2009. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Model;
@class SyncNotePad;
@class NotePadViewController;                                                   //leg20210701 - TopXNotes2

@interface TopXNotesAppDelegate : NSObject <UIApplicationDelegate,
											UITabBarControllerDelegate,
											UIActionSheetDelegate, NSNetServiceDelegate>	//leg20110523 - 1.0.4
{
    UIWindow *window;
    UITabBarController *tabBarController;
	
	IBOutlet Model *model;														//leg20110523 - 1.0.4
	BOOL syncEnabled;															//leg20110523 - 1.0.4
	BOOL firstBecameActive;														//leg20110523 - 1.0.4
    BOOL notepadWasReplaced;                                                    //leg20210705 - TopXNotes 1.6
	SyncNotePad *syncNotePad;													//leg20110523 - 1.0.4
	
	NSMutableDictionary *savedSettingsDictionary;								//leg20110523 - 1.0.4
}

@property (nonatomic, strong) IBOutlet SyncNotePad *syncNotePad;			    //leg20110523 - 1.0.4
@property (nonatomic, strong) IBOutlet Model *model;							//leg20110523 - 1.0.4

@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UITabBarController *tabBarController;
@property (nonatomic, strong) IBOutlet NotePadViewController *notePadViewController;    //leg20210701 - TopXNotes2
@property BOOL notepadWasReplaced;                                              //leg20210705 - TopXNotes 1.6

@end
