//
//  AboutViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 1/15/10.
//  Copyright Tropical Software 2010. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210630 - TopxNotes 1.6
//

#import "AboutViewController.h"
//#import "Constants.h"

@implementation AboutViewController
@synthesize model;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = @"About TopXNotes";
	}
	
	return self;
}


// fetch objects from our bundle based on keys in our Info.plist
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Makes reading About box text easier
    self.view.backgroundColor = [UIColor whiteColor];    // use the table view background color
    
    // Display of information from info.plist and InfoPlist.strings.            //leg202100630 - TopXNotes 1.6
    NSString *bundleName = [self infoValueForKey:@"CFBundleName"];
    NSString *buildNumber = [self infoValueForKey:@"CFBundleVersion"];
    NSString *shortVersion = [self infoValueForKey:@"CFBundleShortVersionString"];
                                                          
    appName.text = [NSString stringWithFormat:@"%@\rVer. %@ (%@)", bundleName, shortVersion, buildNumber];
    copyright.text = [self infoValueForKey:@"NSHumanReadableCopyright"];
    
    // ***JHL 2012.05.11 added for new Done button
    myShinyButton.tintColor = [UIColor darkGrayColor];
}

- (IBAction)dismissAction:(id)sender
{
#pragma unused (sender)

    [self dismissViewControllerAnimated:YES completion:nil];                    //leg20140205 - 1.2.7
}

- (void)viewDidAppear:(BOOL)animated
{
#pragma unused (animated)

    [super viewDidAppear:animated];
    
	// do something here as our view re-appears
}

// Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
- (BOOL)shouldAutorotate
{
    return NO;
}

// Fix for broken iOS 6.0 autorotation.                                     //leg20121220 - 1.2.2
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
