//
//  Model.h
//  TopXNotes
//
//  Created by Lewis Garrett on 4/8/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
/// Changes:
//
//leg20150318 - 1.5.0 - iOS - Fix for sort by title and date no longer working
//                      with iOS 8.2.  Changed sortUsingFunction comparison
//                      function proto type from static int sortByDate(Note… to
//                      NSComparisonResult sortByDate(Note…
//leg20130205 - 1.3.0 - Added: - (BOOL)hasEncryptedNotes;
//
//leg20121223 - 1.3.0 - Increased notepad version (kNotePadVersionCurrent) to 4.
//
//leg20121022 - 1.2.2 - Added - (int)sortNotesByTitle:(BOOL)ascending;
//
//leg20120911 - 1.7.5 - #ifdef NOTEPAD_MODEL_PATH_IS_MAC added to control the path
//                      to the Model whether on Mac or iOS device because Apple
//                      requires us to use the Application Support directory when
//                      running on MacOS and the Documents directory is still used
//                      on iOS.
//
//leg20120911 - 1.7.5 - Added syncNextNewNoteID to Model.
//leg20120207 - 1.7.5 - Added notePadSyncDate to Model.
//
//*leg - 2010-05-01 - Re-implemented Model so that other data besides noteList can be part of it.
//						NotePad object now contains instances of noteList and additional data items.
//						Model object now contains one instance of NotePad object.  Changed loadData
//						and saveData so that individual objects are key archived/unarchived.
//


// Constants
#define kNotePadVersion0		-1                                              //leg20120207 - 1.7.5
#define kNotePadVersion1		1                                               //leg20120207 - 1.7.5
#define kNotePadVersion2		2                                               //leg20120207 - 1.7.5
#define kNotePadVersion3        3                                               //leg20120207 - 1.7.5
#define kNotePadVersion4        4                                               //leg20121223 - 1.3.0
#define kNotePadVersionCurrent	kNotePadVersion4                                //leg20121223 - 1.3.0

// Keys to note pad archive data
#define KEY_NotePadVersion		@"KEY_NotePadVersion"                           //leg20120809 - 1.7.5
#define KEY_SyncNextNewNoteID	@"KEY_SyncNextNewNoteID"                        //leg20120911 - 1.7.5
#define KEY_NotePadSyncDate		@"KEY_NotePadSyncDate"                          //leg20120207 - 1.7.5
#define KEY_DeviceUDID			@"KEY_DeviceUDID"
#define KEY_RealDeviceUDID		@"KEY_RealDeviceUDID"                           //leg20120403 - 1.7.3
#define KEY_ProductCode			@"KEY_ProductCode"
#define KEY_NoteList			@"KEY_NoteList"

@class Model;
@class Note;

@interface NotePad : NSObject {
	NSNumber*			notePadVersion;                                         //leg20120809 - 1.7.5
    NSNumber*           syncNextNewNoteID;                                      //leg20120911 - 1.7.5
    NSDate*             notePadSyncDate;                                        //leg20120207 - 1.7.5
	NSMutableString*	deviceUDID;
	NSMutableString*	realDeviceUDID;                                         //leg20120403 - 1.7.3
	NSMutableString*	productCode;
	NSMutableArray*		noteList;
}

@property (nonatomic, strong) NSNumber* notePadVersion;                         //leg20120911 - 1.7.5
@property (nonatomic, strong) NSNumber* syncNextNewNoteID;                      //leg20120809 - 1.7.5
@property (nonatomic, strong) NSDate* notePadSyncDate;                          //leg20120207 - 1.7.5
@property (nonatomic, strong) NSMutableString* deviceUDID;
@property (nonatomic, strong) NSMutableString* realDeviceUDID;                  //leg20120403 - 1.7.3
@property (nonatomic, strong) NSMutableString* productCode;
@property (nonatomic, strong) NSMutableArray* noteList;

@end

@interface Model : NSObject {
	NotePad* notePad;
    
    bool             NotePad_Version_Changed;

    NSURL *appGroupSubDirectoryURL;                                             //leg20210712 - TopxNotes 1.6
}
- (void)setDeviceUDID:(NSString*)deviceUDID;
- (NSString*)deviceUDID;

- (void)setRealDeviceUDID:(NSString*)deviceUDID;                                //leg20120403 - 1.7.3
- (NSString*)realDeviceUDID;                                                    //leg20120403 - 1.7.3

- (void)setDeviceLastSyncDate:(NSDate*)deviceSyncDate;                          //leg20120207 - 1.7.5
- (NSDate*)deviceLastSyncDate;                                                  //leg20120207 - 1.7.5

- (void)setSyncNextNewNoteID:(NSNumber*)deviceSyncDate;                         //leg20120911 - 1.7.5
- (NSNumber*)syncNextNewNoteID;                                                 //leg20120911 - 1.7.5

- (NSNumber*)notePadVersion;                                                    //leg20120207 - 1.7.5

- (void)addNote:(Note*)note;
- (void)replaceNoteAtIndex:(NSUInteger)index withNote:(Note*)note;              //leg20121127 - 1.2.2
//- (void)updateNoteForIndex:(Note*)note:(int)index;
- (void)updateNoteAtIndex:(NSUInteger)index withNote:(Note*)note;               //leg20121204 - 1.3.0
//- (int)numberOfNotes;
- (NSInteger)numberOfNotes;                                                     //leg20150318 - 1.5.0
//- (int)sortNotesByDate:(BOOL)ascending;
- (NSUInteger)sortNotesByDate:(BOOL)ascending;                                  //leg20150318 - 1.5.0
//- (int)sortNotesByTitle:(BOOL)ascending;                                        //leg20121022 - 1.2.2
- (NSUInteger)sortNotesByTitle:(BOOL)ascending;                                 //leg20150318 - 1.5.0
- (Note*)getNoteForIndex:(int)index;
- (void)removeNoteAtIndex:(int)index;
- (void)removeAllNotes;                                                         //leg20120303 - 1.7.5
- (BOOL)hasEncryptedNotes;                                                      //leg20130205 - 1.3.0

- (NSString*)pathToData;

- (BOOL)saveData;
- (BOOL)loadData;

@property (nonatomic, strong) NSURL* appGroupSubDirectoryURL;                   //leg20210712 - TopxNotes 1.6

@end
