
//
//  Constants.h
//  TopXNotes_touch
//
//  Created by Lewis Garrett on 07/12/13.
//  Copyright (c) 2013 Tropical Software, Inc. All rights reserved.
//


//leg20120911 - 1.7.5 - #ifdef NOTEPAD_MODEL_PATH_IS_MAC added to control the path
//                      to the Model whether on Mac or iOS device because Apple
//                      requires us to use the Application Support directory when
//                      running on MacOS and the Documents directory is still used
//                      on iOS.  Should be defined only on Mac.
//
//leg20130409 - 1.2.3 - Added VERSION_NOTE_SYNC to control "pairing-up" the MacOS
//                      and iOS versions of TopXNotes in order to prevent syncing
//                      between incompatible versions.
//leg20131112 - 1.8.0 - Changed VERSION_NOTE_SYNC to 2 so that this will be a
//                      paired release with

#pragma mark -
#pragma mark Constants
#pragma mark -

//#define NOTEPAD_MODEL_PATH_IS_MAC 1

//#define VERSION_NOTE_SYNC           1
#define VERSION_NOTE_SYNC           2                                           //leg20131112 - 1.8.0/1.2.6

#define kCustomButtonHeight		30.0

// keys to our dictionary holding info on each page
#define kCellIdentifier			@"MyIdentifier"
#define kViewControllerKey		@"viewController"
#define kTitleKey				@"title"
#define kExplainKey				@"explainText"
#define kModelFileName			@"TopXNotes.dat"
#define kReceivedModelFileName	@"Received.dat"
#define kBackupModelFileName	@"Backup.dat"

// Settings keys
#define kRestoreDataDictionaryKey	@"RestoreDataDictionaryKey"	// Settings - Restore Backups dictionary key 
#define kProductCode_Part1_Key		@"ProductCode_Part1_Key"	// TopXNotes Product Key part 1 key
#define kProductCode_Part2_Key		@"ProductCode_Part2_Key"	// TopXNotes Product Key part 2 key
#define kProductCode_Part3_Key		@"ProductCode_Part3_Key"	// TopXNotes Product Key part 3 key
#define kProductCode_Part4_Key		@"ProductCode_Part4_Key"	// TopXNotes Product Key part 4 key
#define kProductCode_OwnerName_Key	@"ProductCode_OwnerName_Key"	// TopXNotes Product Key Owner Name key

#define kNextAutoBackupNumber_Key	@"NextAutoBackupNumber_Key"	// Number of the next automatic notepad backup 
#define kAutoBackupsArray_Key		@"AutoBackupsArray_Key"		// Ordered array of file names of automatic notepad file backups

#define kNoteFontName_Key			@"NoteFontName_Key"			// Note Preference - Font Name key
#define kNoteFontSize_Key			@"NoteFontSize_Key"			// Note Preference - Font Size key

#define kSortType_Key               @"SortType_Key"             // Notepad Preference - Sort Type key             //leg20121022 - 1.2.2
#define kShowNotepaper_Key          @"ShowNotepaper_Key"        // Notepad Preference - Show/Hide Notepaper key   //leg20140222 - 1.2.7

#define kSyncEnabledOrDisabled_Key	@"SyncEnabledOrDisabled_Key" // Sync Preference - Enabled/Disabled key        //leg20110502 - 1.0.4

#define kUUID_GENERATED_KEY @"UUID_Generated_Key"                               //leg20120327 - 1.2.0

// Notification names.                                                          //leg20210626 - TopxNotes 1.6
#define kNotification_Refresh_NoteList @"Refresh_NoteList"
#define kSplitView_Back_Button_Hit @"SplitView_Back_Button_Hit"

// Identifiers and widths for the various components of NoteFontPickerController
#define FONT_NAME_COMPONENT 0
#define FONT_NAME_COMPONENT_WIDTH 200
#define FONT_NAME_LABEL_WIDTH 60

#define FONT_SIZE_COMPONENT 1
#define FONT_SIZE_COMPONENT_WIDTH 100
#define FONT_SIZE_LABEL_WIDTH 56

#define kNumberOfAutoBackups		15

#define kDefaultSortType NotesSortByDateDescending       

#define kNoteTextFont			@"Helvetica"                                    //leg20130109 - 1.2.2
#define kNoteTextFontSize		18

#define kNoteTitleMaxLength		80		// the same length as TopXNotes Mac 

// Sync command ID definitions                                                  //leg20120308 - 1.2.0
#define	kSTART_SYNC             0x47471002                                      
#define	kSTART_SYNCV1           0x47471001                                      
#define	kSYNC_INFO              0x47471009

// Sync version definitions                                                     //leg20130805 - 1.2.6
#define	kNOTE_SYNC_VER_CURRENT	kNOTE_SYNC_VER_002                              //leg20130910 - 1.2.6
#define	kNOTE_SYNC_VER_000      0x00000000
#define	kNOTE_SYNC_VER_001      0x00000001
#define	kNOTE_SYNC_VER_002      0x00000002
                                      
// Sync error codes                                                             //leg20130805 - 1.2.6
#define	kSYNC_NOERROR           0x00000000
#define	kSYNC_ERROR_001         0x00000001      // Mac NoteSync version > iOS
#define	kSYNC_ERROR_002         0x00000002      // Mac NoteSync version < iOS
#define	kSYNC_ERROR_003         0x00000003      // Mac did not resolve service

// Sync buffer size
#define		kBufferLength	 1448

#pragma mark -
#pragma mark Struct definitions
#pragma mark -

// Sort Notes type                                                              //leg20121017 - 1.2.2
typedef enum {
    NotesSortByDateAscending = 0,
    NotesSortByDateDescending,
    NotesSortByTitleAscending,
    NotesSortByTitleDescending
} NotesSortType;

// Sync control info
struct Sync_Info {
    uint16_t                version;
    uint16_t                code;
};

// Struct definitions
union SecondaryValue {
    uint32_t            notepad_Size;			
    struct Sync_Info	code_Value;
};

// Sync control record
struct Sync_Record {
    uint32_t                command_ID;
    union SecondaryValue	secondary;
};

// Re-define stream buffer as both a block of notepad data and as a sync control record.
union BufferRedefined {
    uint8_t             dataBytes[500000];
    struct Sync_Record	syncRecord;
};

#pragma mark -
#pragma mark Class Methods
#pragma mark -

@interface Constants : NSObject

+ (NSData *)FileDataEOF;
//+ (NSString *) guid;
+ (BOOL) validNetworkConnection;


// Color classes.                                                               //leg20210626 - TopxNotes 1.6
+ (UIColor *) waterColor;
+ (UIColor *) backgroundThemeColor;
+ (UIColor *) controlsColor;
@end
