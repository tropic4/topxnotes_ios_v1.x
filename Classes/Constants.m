//
//  Constants.m
//  TopXNotes_touch
//
//  Created by Lewis Garrett on 07/12/13.
//  Copyright (c) 2013 Tropical Software, Inc. All rights reserved.
//

#import "Constants.h"
#import <SystemConfiguration/SystemConfiguration.h>

#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>

@implementation Constants

#pragma mark -
#pragma mark File Data End-Of-File marker

// An end-of-file marker
+ (NSData *)FileDataEOF
{
    return [NSData dataWithBytes:"\x47\x47\x0E\x0F" length:4];
}

//+ (NSString*) guid
//{
//    CFUUIDRef	uuidObj = CFUUIDCreate(nil);//create a new UUID
//                                            //get the string representation of the UUID
//    NSString	*uuidString = (__bridge_transfer NSString*)CFUUIDCreateString(nil, uuidObj);
//    CFRelease(uuidObj);
//    return uuidString;
//}

#pragma mark -
#pragma mark NetworkConnection

//The following was adapted from the Rechability Apple sample
+ (BOOL) validNetworkConnection
{
    struct sockaddr_in zeroAddress;
	bzero(&zeroAddress, sizeof(zeroAddress));
	zeroAddress.sin_len = sizeof(zeroAddress);
	zeroAddress.sin_family = AF_INET;
    
	SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    
    SCNetworkReachabilityFlags flags = 0;
    Boolean bFlagsValid = SCNetworkReachabilityGetFlags(reachability, &flags);
    CFRelease(reachability);
    
    if (!bFlagsValid)
        return NO;
    
    if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
	{
		// if target host is not reachable
		return NO;//NotReachable;
	}
    
	BOOL retVal = NO;
	
	if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
	{
		// if target host is reachable and no connection is required
		//  then we'll assume (for now) that your on Wi-Fi
		retVal = YES;
	}
	
	
	if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
         (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
	{
        // ... and the connection is on-demand (or on-traffic) if the
        //     calling application is using the CFSocketStream or higher APIs
        
        if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
        {
            // ... and no [user] intervention is needed
            retVal = YES;
        }
    }
	
	if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
	{
		// ... but WWAN connections are OK if the calling application
		//     is using the CFNetwork (CFSocketStream?) APIs.
		retVal = YES;
	}
    
	return retVal;
}

// Color classes copied from TopXNotes2.                                        //leg20210626 - TopxNotes 1.6
+ (UIColor *) backgroundThemeColor
{
    static UIColor * g_backgroundThemeColor;
    if(!g_backgroundThemeColor)
    {
//        g_backgroundThemeColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"background.png"]];
        g_backgroundThemeColor = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"XPalm.png"]];
    }
    return g_backgroundThemeColor;
}

+ (UIColor *) waterColor                                                        //leg20150114 - 1.5.0
{
    static UIColor * g_waterColor; // This is the theme color
    if(!g_waterColor)
    {
        g_waterColor = [UIColor colorWithRed:33.f/255 green:142.f/255 blue: 199.f/255 alpha: 1];       // native
    }
    return g_waterColor;
}

+ (UIColor *) controlsColor
{
    static UIColor * g_controlsColor; // This is the theme color
    if(!g_controlsColor)
    {
        g_controlsColor = [UIColor colorWithRed:128.f/255 green:255.f/255 blue: 0.f/255 alpha: 1];       // native
    }
    return g_controlsColor;
}
@end
