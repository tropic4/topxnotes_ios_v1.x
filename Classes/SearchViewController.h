//
//  SearchViewController.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/7/09.
//  Copyright 2009 Iota. All rights reserved.
//


#import <UIKit/UIKit.h>
@class Model;

@interface SearchViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
	IBOutlet UITableView		*myTableView;
	IBOutlet UISearchBar		*mySearchBar;
	
	IBOutlet Model* model;

	NSMutableArray				*listContent;			// the master content
	NSMutableArray				*filteredListContent;	// the filtered content as a result of the search
	NSMutableArray				*savedContent;			// the saved content in case the user cancels a search
    
    int noteIndex;                                                              //leg20210628 - TopXNotes 1.6

}

@property (nonatomic, strong) Model *model;

@property (nonatomic, strong) UITableView *myTableView;
@property (nonatomic, strong) UISearchBar *mySearchBar;

@property (nonatomic, strong) NSMutableArray *listContent;
@property (nonatomic, strong) NSMutableArray *filteredListContent;
@property (nonatomic, strong) NSMutableArray *savedContent;

@end
