//
//  NoteSettingsViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 4/15/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210630 - TopxNotes 1.6
//

#import "NoteSettingsViewController.h"
#import "NoteFontPickerController.h"
#import "Formatter.h"
#import "Constants.h"

//#define kRestoreDataDictionaryKey	@"RestoreDataDictionaryKey"	// Settings dictionary key 

@implementation NoteSettingsViewController


@synthesize pickerViewContainer;
@synthesize noteFontPickerController;
@synthesize noteFontPickerViewContainer;

@synthesize model;

NSComparisonResult familyNameSort(id string1, id string2, void *context)
{
#pragma unused (context)

	return [string1 localizedCaseInsensitiveCompare:string2];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = @"Note Preferences";
	}
	
	return self;

// Note that model is not yet valid here -- reference it from -viewDidAppear
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)

    [super viewWillAppear:animated];

	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)  
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];

	// Build a sorted array of all available Font Names For Family Names.
	NSMutableArray *allFontNamesForFamilys = [NSMutableArray array];
	NSMutableArray *familyFontNamesArray = (NSMutableArray*)[UIFont familyNames];

	NSEnumerator *enumerator = [familyFontNamesArray objectEnumerator];
	id anObject;
	 
	while (anObject = [enumerator nextObject]) {
		[allFontNamesForFamilys addObject:anObject];	// Add family name
		
		NSArray *fontNamesForFamily = [UIFont fontNamesForFamilyName:anObject];
		
		NSEnumerator *enumerator2 = [fontNamesForFamily objectEnumerator];
		id anObject2;
		 
		while (anObject2 = [enumerator2 nextObject]) {
			[allFontNamesForFamilys addObject:anObject2];	// Add font names of family
		}
	}

	noteFontPickerController.fontNamesArray = (NSMutableArray *)[allFontNamesForFamilys sortedArrayUsingFunction:familyNameSort context:NULL];

	// Build an array of selectable Font Sizes.
	noteFontPickerController.fontSizesArray = [NSArray arrayWithObjects:@"8", @"10", @"12", @"14", @"16", @"18",@"20", @"22", @"24",@"26", @"28", @"30",nil];

	// Assemble the picker view
	[pickerViewContainer addSubview:noteFontPickerViewContainer];
	
	// Get Font data from saved settings
	NSInteger fontNameRow = 0;
	NSString * fontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
	if (fontName != nil) {
		fontNameRow = [noteFontPickerController.fontNamesArray indexOfObject:fontName];
		if (fontNameRow == NSNotFound)
			fontNameRow = 0;
	}
	
	NSInteger fontSizeRow = 0;
	NSString * fontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
	if (fontSize != nil) {
		fontSizeRow = [noteFontPickerController.fontSizesArray indexOfObject:fontSize];
		if (fontSizeRow == NSNotFound)
			fontSizeRow = 0;
	}
	
	// Select the saved setting font and size in the picker
	[noteFontPickerController.pickerView selectRow:fontNameRow inComponent:FONT_NAME_COMPONENT animated:NO];
	[noteFontPickerController.pickerView selectRow:fontSizeRow inComponent:FONT_SIZE_COMPONENT animated:NO];

	// Update the label with the current font name
	noteFontPickerController.label.text = [NSString stringWithFormat:@"%@ - %@ pt", [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT],
																					[noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontSizeRow forComponent:FONT_SIZE_COMPONENT]];
	NSString *font = [noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontNameRow forComponent:FONT_NAME_COMPONENT];
//	int size = [[noteFontPickerController pickerView:noteFontPickerController.pickerView titleForRow:fontSizeRow forComponent:FONT_SIZE_COMPONENT] intValue];
	noteFontPickerController.label.font = [UIFont fontWithName:font
//                                                          size:size];
                                                          size:16.0];           //leg20210519 - TopXNotes2
    
    // Show the "Point" label appropriate to the version of iOS.                //leg20140205 - 1.2.7
    if (IS_OS_7_OR_LATER) {
        _iOS6PointLabel.hidden = YES;
        _iOS7PointLabel.hidden = NO;
    } else {
        _iOS6PointLabel.hidden = NO;
        _iOS7PointLabel.hidden = YES;
    }
    
	// Make sure the picker has access to the notepad
	noteFontPickerController.model = model;

// Obsolete.                                                                    //leg20210519 - TopXNotes2
//	// make sure save button is positioned appropriate to device orientation
//	UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
//	[self positionRestoreButton:deviceOrientation];
}

// Obsolete.                                                                    //leg20210519 - TopXNotes2
//- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
//#pragma unused (duration)
//
//	[self positionRestoreButton:toInterfaceOrientation];
//}

// Obsolete.                                                                    //leg20210519 - TopXNotes2
//- (void)positionRestoreButton:(UIInterfaceOrientation)toInterfaceOrientation {
//
//    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation)) {
//		noteFontPickerController.saveFontButton.frame = landscapeLocRestoreButton;
////        if (IS_OS_7_OR_LATER)                                                   //leg20140205 - 1.2.7
////            noteFontPickerController.label.frame = landscapeLocFontSampleRect;
//	} else {
//		noteFontPickerController.saveFontButton.frame = originalLocRestoreButton;
////        if (IS_OS_7_OR_LATER)                                                   //leg20140205 - 1.2.7
////            noteFontPickerController.label.frame = originalLocFontSampleRect;
//	}
//}

- (void)viewDidLoad {
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;

	// make sure Restore button is positioned appropriate to device orientation
    [super viewDidLoad];
	
    // This will appear as the title in the navigation bar.                     //leg20210519 - TopXNotes2
    self.title = @"Note Preferences";

//    // Pretty up buttons a little.                                              //leg20210514 - TopXNotes2
//    CALayer *viewLayer = [noteFontPickerController.pickerView layer];
////    viewLayer.borderColor = [[Constants controlsColor] CGColor];
//    viewLayer.borderColor = [[UIColor redColor] CGColor];
//    viewLayer.borderWidth = 10.0f;
//    viewLayer.masksToBounds = YES;
//    viewLayer.cornerRadius = 5.0f;
//
//    // Pretty up buttons a little.                                              //leg20210514 - TopXNotes2
//    CALayer *viewLayer2 = [pickerViewContainer layer];
////    viewLayer.borderColor = [[Constants controlsColor] CGColor];
//    viewLayer2.borderColor = [[UIColor greenColor] CGColor];
//    viewLayer2.borderWidth = 1.0f;
//    viewLayer2.masksToBounds = YES;
//    viewLayer2.cornerRadius = 5.0f;
    
   // Obsolete.                                                                    //leg20210519 - TopXNotes2
//	// Save original portrait device orientation location of Set Font button before positioning
//	//	and calculate the landscape device orientation location of Set Font button
//	originalLocRestoreButton = noteFontPickerController.saveFontButton.frame;
//	landscapeLocRestoreButton = CGRectMake(noteFontPickerController.pickerView.frame.origin.x + noteFontPickerController.pickerView.frame.size.width + 8,
//										noteFontPickerController.pickerView.frame.origin.y + (noteFontPickerController.pickerView.frame.size.height/2) - 16,
//										noteFontPickerController.saveFontButton.frame.size.width,
//										noteFontPickerController.saveFontButton.frame.size.height);
//
////	// Save original portrait device orientation location of Font Sample Label   //leg20140205 - 1.2.7
////    //  before positioning and calculate the landscape device orientation
////    //  location of Font Sample Label.
////    originalLocFontSampleRect = noteFontPickerController.label.frame;
////	landscapeLocFontSampleRect = CGRectMake(noteFontPickerController.pickerView.frame.origin.x + noteFontPickerController.pickerView.frame.size.width,
////                                           noteFontPickerController.pickerView.frame.origin.y + (noteFontPickerController.pickerView.frame.size.height) - 40,
////                                           noteFontPickerController.label.frame.size.width,
////                                           noteFontPickerController.label.frame.size.height);
//
//    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
//	[self positionRestoreButton:deviceOrientation];
	
// This is now handled by segue @"Note_Preferences_Info_Segue".                 //leg20210419 - TopXNotes2
//	// Prepare our Info button controller
//	modalViewController = [[NoteSettingsInfoViewController alloc] initWithNibName:@"NoteSettingsInfoViewController" bundle:nil];
//	modalViewController.model = self.model;
//	modalViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	
	// add our custom right button to show our modal view controller
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
	self.navigationItem.rightBarButtonItem = modalButton;
                                                          //leg20140216 - 1.2.7
}

// user clicked the "i" button, present a modal UIViewController
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)

// This is now handled by segue @"Note_Preferences_Info_Segue".                 //leg20210419 - TopXNotes2
//	// present as a modal child or overlay view
////	[[self navigationController] presentModalViewController:modalViewController animated:YES];
//    [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];    //leg20140205 - 1.2.7
    [self performSegueWithIdentifier:@"Note_Preferences_Info_Segue" sender: self];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations

    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;	// portrait and landscape
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

	self.pickerViewContainer = nil;
	
	self.noteFontPickerViewContainer = nil;
	
	[super viewDidUnload];
}


- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}


@end
