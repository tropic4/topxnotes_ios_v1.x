//
//  BackupSettingsViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import "BackupSettingsViewController.h"
#import "BackupPickerController.h"
#import "Formatter.h"
#import "Constants.h"

//#define kRestoreDataDictionaryKey	@"RestoreDataDictionaryKey"	// Settings dictionary key 

@implementation BackupSettingsViewController

@synthesize pickerViewContainer;
@synthesize backupPickerController;
@synthesize backupPickerViewContainer;

@synthesize model;

// Obsolete.                                                                    //leg20210518 - TopXNotes2
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self)
	{
		// this will appear as the title in the navigation bar
		self.title = @"Backup/Restore";
	}
	
	return self;

//•leg - 06/09/09 -  Note that model is not yet valid here -- reference it from -viewDidAppear
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];

    // this will appear as the title in the navigation bar                      //leg20210517 - TopXNotes2
    self.title = @"Restore Backup";

    // Reading Defaults 
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)  
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
    // Provide view controller for presenting alert.                            //leg20210420 - TopXNotes2
    backupPickerController.backupSettingsViewController = self;


	// Get auto backup notepad data
//	backupPickerController.backupDescriptionsArray = [[NSMutableArray array] retain];
	// Eliminate "Potential leak of an object" message from analyzer            //leg20140216 - 1.2.7
	backupPickerController.backupDescriptionsArray = [NSMutableArray array];    //leg20140216 - 1.2.7

	NSArray *array = [dict objectForKey: kAutoBackupsArray_Key];
	if (array != nil) {

		// Prepare to retreive backup file descriptions
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error = nil;

		NSEnumerator *enumerator = [array objectEnumerator];
		id anObject;
		NSMutableString *pathToBackupModelDataFile;
		NSMutableString *backupDescription = nil;
		NSDictionary *fileAttributesDictionary;
		NSDate *modificationDate;
		while (anObject = [enumerator nextObject]) {
			pathToBackupModelDataFile = (NSMutableString*)[self pathToAutoBackup:anObject];

			fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToBackupModelDataFile error:&error];
			modificationDate = [fileAttributesDictionary fileModificationDate];
			NSRange range = [anObject rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"_"]];
			if (range.location != NSNotFound)
				backupDescription = (NSMutableString*)[anObject substringToIndex:range.location];

			// Add the backup description to the picker list
//			backupDescription = [NSString stringWithFormat:@"%@ - %@", backupDescription, [NSString longDate:modificationDate]];
			backupDescription = (NSMutableString*)[NSString                     //leg20140216 - 1.2.7
                                    stringWithFormat:@"%@ - %@",
                                    backupDescription,
                                    [NSString longDate:modificationDate]];
			[backupPickerController.backupDescriptionsArray addObject:backupDescription];
		}
	}
	
	// Assemble the picker view
	[pickerViewContainer addSubview:backupPickerViewContainer];
	
	// Select the oldest backup
	[backupPickerController.pickerView selectRow:0 inComponent:0 animated:NO];

//	// Update the label with the oldest backup description
//	backupPickerController.label.text = [backupPickerController pickerView:backupPickerController.pickerView titleForRow:0 forComponent:0];
    // Update the restore button with the oldest backup description.            //leg20210518 - TopXNotes2
    backupPickerController.backup2Restore = [backupPickerController pickerView:backupPickerController.pickerView titleForRow:0 forComponent:0];
    NSString *restoreButtonTitle = [NSString stringWithFormat:@"Restore: %@", backupPickerController.backup2Restore];
    [backupPickerController.restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];
    [backupPickerController.restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];

	// Make sure the picker has access to the notepad
	backupPickerController.model = model;

	// make sure Restore button is positioned appropriate to device orientation
//	UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];    //leg20210518 - TopXNotes2
	[self positionRemoveButton:deviceOrientation];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
#pragma unused (duration)
    
	[self positionRemoveButton:toInterfaceOrientation];
}

- (void)positionRemoveButton:(UIInterfaceOrientation)toInterfaceOrientation {

//    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation)) {
//		backupPickerController.restoreButton.frame = landscapeLocRestoreButton;
//	} else {
//		backupPickerController.restoreButton.frame = originalLocRestoreButton;
//	}
    
    // Position "Remove All Notes" button depending on device orientation.      //leg20210518 - TopXNotes2
//    if (UIDeviceOrientationIsLandscape(toInterfaceOrientation)) {
    if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {            //leg20210518 - TopXNotes2
        backupPickerController.removeButton.frame = landscapeLocRemoveButton;
    } else {
        backupPickerController.removeButton.frame = originalLocRemoveButton;
    }
}

- (void)viewDidLoad {
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
     self.navigationItem.rightBarButtonItem = self.editButtonItem;

//	// make sure Restore button is positioned appropriate to device orientation
//    [super viewDidLoad];
//
//	// Save original portrait device orientation location of Restore button before positioning
//	//	and calculate the landscape device orientation location of Restore button
//	originalLocRestoreButton = backupPickerController.restoreButton.frame;
//	landscapeLocRestoreButton = CGRectMake(backupPickerController.pickerView.frame.origin.x + backupPickerController.pickerView.frame.size.width + 8,
//										backupPickerController.pickerView.frame.origin.y + (backupPickerController.pickerView.frame.size.height/2) - 16,
//										backupPickerController.restoreButton.frame.size.width,
//										backupPickerController.restoreButton.frame.size.height);
//    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
//	[self positionRestoreButton:deviceOrientation];

    // Position "Remove All Notes" button depending on device orientation.      //leg20210518 - TopXNotes2
    [super viewDidLoad];
    
    // Save original portrait device orientation location of Remove button
    //  before positioning and calculate the landscape device orientation
    //  location of Restore button
    originalLocRemoveButton = backupPickerController.removeButton.frame;
//    landscapeLocRemoveButton = CGRectMake(backupPickerController.pickerView.frame.origin.x + backupPickerController.pickerView.frame.size.width + 8,
//                                        backupPickerController.pickerView.frame.origin.y + (backupPickerController.pickerView.frame.size.height/2) - 16,
//                                        backupPickerController.removeButton.frame.size.width,
//                                        backupPickerController.removeButton.frame.size.height);
    CGFloat midViewX = CGRectGetMidX(backupPickerController.pickerView.frame);
    CGFloat midButtonX = backupPickerController.removeButton.frame.size.width/2.0;
    landscapeLocRemoveButton = CGRectMake(backupPickerController.pickerView.frame.origin.x+midViewX-midButtonX,
                                        backupPickerController.pickerView.frame.origin.y,
                                        backupPickerController.removeButton.frame.size.width,
                                        backupPickerController.removeButton.frame.size.height);

 //    UIDeviceOrientation deviceOrientation = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation deviceOrientation = [[UIApplication sharedApplication] statusBarOrientation];    //leg20210518 - TopXNotes2
    [self positionRemoveButton:deviceOrientation];

// Consider framing button.
//    CALayer *viewLayer = [backupPickerController.removeButton layer];
////    viewLayer.borderColor = [[Constants controlsColor] CGColor];
//    viewLayer.borderColor = [[UIColor redColor] CGColor];
//    viewLayer.borderWidth = 1.0f;
//    viewLayer.masksToBounds = YES;
//    viewLayer.cornerRadius = 5.0f;

    // This is now handled by segue @"Backup_Restore_Info_Segue".                   //leg20210420 - TopXNotes2
//	// Prepare our Info button controller
//	modalViewController = [[BackupInfoViewController alloc] initWithNibName:@"BackupInfoViewController" bundle:nil];
//	modalViewController.model = self.model;
//	modalViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	
	// add our custom right button to show our modal view controller
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
	self.navigationItem.rightBarButtonItem = modalButton;
                                                          //leg20140216 - 1.2.7
}

// user clicked the "i" button, present a modal UIViewController
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)

// This is now handled by segue @"Backup_Restore_Info_Segue".                   //leg20210420 - TopXNotes2
//	// present as a modal child or overlay view
//    [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];    //leg20140205 - 1.2.7
    [self performSegueWithIdentifier:@"Backup_Restore_Info_Segue" sender: self];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations

    return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;	// portrait and landscape
}


- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;

	self.pickerViewContainer = nil;
	
	self.backupPickerViewContainer = nil;
	
	[super viewDidUnload];
}


- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}


@end
