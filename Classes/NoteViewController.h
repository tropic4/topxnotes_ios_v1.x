//
//  NoteViewController.h
//  TopXNotes_touch
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "NotePaperView.h"                                                       //leg20110419 - 1.0.4

@class Note;
@class Model;
@class HoverView;                                                               //leg20110421 - 1.0.4

extern NSString *Show_HoverView;                                                //leg20110421 - 1.0.4


@interface NoteViewController : UIViewController <UITextFieldDelegate,
													NotePaperViewDelegate,      //leg20110419 - 1.0.4
													UIGestureRecognizerDelegate,//leg20110421 - 1.0.4
													UITextViewDelegate,
													UIAlertViewDelegate,
													UIActionSheetDelegate,
													MFMailComposeViewControllerDelegate> {
	
	IBOutlet UILabel *dateLabel;
	IBOutlet UITextView *noteText;
	IBOutlet UIBarButtonItem *doneButton;
	IBOutlet UIButton *emailButton;
	IBOutlet UIButton *toolsHUDButton;                                          //leg20110531 - 1.0.4
	IBOutlet UISlider *slider;                                                  //leg20110421 - 1.0.4
	IBOutlet HoverView *hoverView;                                              //leg20110421 - 1.0.4
	
	NSTimer* myTimer;                                                           //leg20110421 - 1.0.4
    UITapGestureRecognizer *tapRecognizer;                                      //leg20110421 - 1.0.4
	UISwipeGestureRecognizer *swipeLeftRecognizer;                              //leg20110421 - 1.0.4
	UILongPressGestureRecognizer *longPressRecognizer;                          //leg20110421 - 1.0.4

	NSString	*dateString;
	
	NSNumber	*syncFlag;
	NSNumber	*noteID;
	NSNumber	*needsSyncFlag;
    NSDate      *noteCreationDate;                                              //leg20140112 - 1.2.7
    
	BOOL	noteIsDirty;
	BOOL	keyboardShown;
	BOOL	emailInProgress;
    BOOL    showNotepaper;                                                      //leg20140222 - 1.2.7
    
	int		noteIndex;
	Model*	__weak model;

	int fontSliderValue; // 0 to 100                                            //leg20110419 - 1.0.4
	NotePaperView *notePaperView;                                               //leg20110419 - 1.0.4
	
	NSMutableDictionary *savedSettingsDictionary;                               //leg20110420 - 1.0.4
	NSMutableString *__weak noteFontName;                                              //leg20110420 - 1.0.4
	NSMutableString *__weak noteFontSize;                                              //leg20110420 - 1.0.4

    UIBarButtonItem *barButtonItem;                                             //leg20140205 - 1.2.7
}

//@property int fontSliderValue;                                                //leg20110419 - 1.0.4
@property (nonatomic) int fontSliderValue;                                      //leg20140212 - 1.2.7
@property (strong) IBOutlet UISlider *slider;                                   //leg20110419 - 1.0.4
@property (strong) IBOutlet NotePaperView *notePaperView;                       //leg20110419 - 1.0.4
@property (nonatomic, weak) Model* model;
@property (nonatomic, weak) NSMutableString *noteFontName;                    //leg20110420 - 1.0.4
@property (nonatomic, weak) NSMutableString *noteFontSize;                    //leg20110420 - 1.0.4

@property (nonatomic, strong) UIButton *emailButton;                            //leg20110421 - 1.0.4
@property (nonatomic, strong) UIButton *toolsHUDButton;                         //leg20110531 - 1.0.4
@property (nonatomic, strong) HoverView *hoverView;                             //leg20110421 - 1.0.4
@property (nonatomic, strong) UITapGestureRecognizer *tapRecognizer;            //leg20110421 - 1.0.4
@property (nonatomic, strong) UISwipeGestureRecognizer *swipeLeftRecognizer;    //leg20110421 - 1.0.4
@property (nonatomic, strong) UILongPressGestureRecognizer *longPressRecognizer; //leg20110421 - 1.0.4
@property (nonatomic, strong) NSDate *noteCreationDate;                         //leg20140112 - 1.2.7
@property (nonatomic, strong) UIBarButtonItem *barButtonItem;                   //leg20140112 - 1.2.7

// Make noteIndex a Property so it is accessable to segue.                      //leg20210626 - TopxNotes 1.6
@property (nonatomic) int noteIndex;

- (void)registerForKeyboardNotifications;
- (id)initWithNibName:(NSString *)nibNameOrNil noteIndex:(int)index;

-(IBAction)fontSliderValueChanged:(UISlider *)sender;                           //leg20110419 - 1.0.4
-(IBAction)done;
-(IBAction)emailNote;													
-(IBAction)toggleToolsHUD:(id)sender;                                           //leg20140205 - 1.2.7

- (void)showHoverView:(BOOL)show;
- (void)showViewNotif:(NSNotification *)aNotification;

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;

// Action Sheet
- (void)dialogOKCancelAction;

// Email
-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;

@end
