//
//  SearchViewController.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/7/09.
//  Copyright 2009 Iota. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210628 - TopxNotes 1.6
//

#import "TopXNotesAppDelegate.h"
#import "NoteViewController.h"
#import "SearchViewController.h"
#import "Model.h"
#import "Note.h"
#import "NoteListCell.h"                                                        //leg20140216 - 1.2.7

@implementation SearchViewController

@synthesize model, listContent, filteredListContent, savedContent, myTableView, mySearchBar;


// Moved code to -viewDidLoad because view is now in Storyboard.                //leg20210628 - TopXNotes 1.6
//- (void)awakeFromNib
//{
//    [super awakeFromNib];
//
//    // Hook-up to model which is now owned by AppDelegate.
//    id appDelegate = [[UIApplication sharedApplication] delegate];
//    self.model = [(TopXNotesAppDelegate*)appDelegate model];
//
//    // Create the master list of notes
//    NSInteger noteCount = [model numberOfNotes];
//	NSMutableArray *noteIndexesArray = [[NSMutableArray alloc] init];
//
//	for (int index=0; index<noteCount; index++)
//	{
//		[noteIndexesArray addObject: [NSNumber numberWithInt:index]];
//	}
//	listContent = [[NSMutableArray alloc] initWithArray:noteIndexesArray];
//
//	// create our filtered list that will be the data source of our table
//	filteredListContent = [[NSMutableArray alloc] initWithCapacity: [listContent count]];
//
//	// this stored the current list in case the user cancels the filtering
//	savedContent = [[NSMutableArray alloc] initWithCapacity: [listContent count]];
//
//	// don't get in the way of user typing
//	mySearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
//	mySearchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
//	mySearchBar.showsCancelButton = NO;
//}

#pragma mark UIViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Make the navigation bar a darker color so controls stand-out.            //leg20210628 - TopXNotes 1.6
    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];

    // Hook-up to model which is now owned by AppDelegate.                      //leg20210628 - TopXNotes 1.6
    id appDelegate = [[UIApplication sharedApplication] delegate];
    self.model = [(TopXNotesAppDelegate*)appDelegate model];

	// Create the master list of notes
    NSInteger noteCount = [model numberOfNotes];
	NSMutableArray *noteIndexesArray = [[NSMutableArray alloc] init];

	for (int index=0; index<noteCount; index++)
	{
		[noteIndexesArray addObject: [NSNumber numberWithInt:index]];
	}
	listContent = [[NSMutableArray alloc] initWithArray:noteIndexesArray];
	
	// create our filtered list that will be the data source of our table
	filteredListContent = [[NSMutableArray alloc] initWithCapacity: [listContent count]];
	
	// this stored the current list in case the user cancels the filtering
	savedContent = [[NSMutableArray alloc] initWithCapacity: [listContent count]]; 
					
	// don't get in the way of user typing
	mySearchBar.autocorrectionType = UITextAutocorrectionTypeNo;
	mySearchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	mySearchBar.showsCancelButton = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
#pragma unused (animated)

    [super viewWillAppear:animated];
    
	// Clear the content array 
	[listContent removeAllObjects];			

	// Reload the master list of notes
    NSInteger noteCount = [model numberOfNotes];
	NSMutableArray *noteIndexesArray = [[NSMutableArray alloc] init];

	for (int index=0; index<noteCount; index++)
	{
		[noteIndexesArray addObject: [NSNumber numberWithInt:index]];
	}
	
	// Load the content list
	[listContent addObjectsFromArray:noteIndexesArray];

	
	NSIndexPath *tableSelection = [myTableView indexPathForSelectedRow];
	[myTableView deselectRowAtIndexPath:tableSelection animated:NO];
}


// Pass information to view controller segued to.                               //leg20210628 - TopXNotes 1.6
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Edit_Found_Note_Segue"] ) {
        NoteViewController *controller = (NoteViewController *)segue.destinationViewController;
        controller.model = self.model;
        controller.noteIndex = noteIndex;
     }
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#pragma unused (tableView)
    
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#pragma unused (tableView, section)
    
	return [filteredListContent count];
}

/*
//•leg - 11/30/09 -accessoryTypeForRowWithIndexPath removed due to following:
2009-11-30 15:07:51.974 TopXNotes[1852:20b] WARNING: Using legacy cell layout due to delegate implementation of tableView:accessoryTypeForRowWithIndexPath: in <SearchViewController: 0x3d1d310>.  Please remove your implementation of this method and set the cell properties accessoryType and/or editingAccessoryType to move to the new cell layout behavior.  This method will no longer be called in a future release.

- (UITableViewCellAccessoryType)tableView:(UITableView *)tableView accessoryTypeForRowWithIndexPath:(NSIndexPath *)indexPath
{
	return UITableViewCellAccessoryDisclosureIndicator;
}
*/

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSInteger row = indexPath.row;

//	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
//	if (cell == nil)
//	{
//		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:@"cellID"] autorelease];
//		cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//	}
    static NSString *CellIdentifier = @"Cell";                                  //leg20140216 - 1.2.7
	NoteListCell *cell = (NoteListCell*)[tableView
                            dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
        cell = [[NoteListCell alloc]
                 initWithReuseIdentifier:CellIdentifier];          
	}
	

	// Set the table cell's characteristics
	NSNumber *val = [filteredListContent objectAtIndex:row];
	int index = [val intValue];
	Note *note = [model getNoteForIndex:index];
	
	//cell.text = note.noteText;	deprecated
	cell.textLabel.text = note.noteText;	
	//cell.textLabel.font =  [UIFont fontWithName:@"Marker Felt" size:16];
	cell.textLabel.font =  [UIFont systemFontOfSize:16];                        //leg20130109 - 1.2.2
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)
    
	NSInteger row = indexPath.row;
	NSNumber *val = [filteredListContent objectAtIndex:row];
	int index = [val intValue];
	
//	// Put the note text in a view
//	NoteViewController *controller = [[NoteViewController alloc]
//									   initWithNibName:@"NoteView" noteIndex:index];
//	controller.model = self.model;
//
//	[[self navigationController] pushViewController:controller animated:YES];
	
    // Since .xibs have been converted to Storyboard so must now use segues.    //leg20210628 - TopXNotes 1.6
    noteIndex = index;
    [self performSegueWithIdentifier:@"Edit_Found_Note_Segue" sender: self];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)
    
    return YES;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
NSLog(@"SearchViewController didReceiveMemoryWarning");
}

#pragma mark UISearchBarDelegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
#pragma unused (searchBar)
    
	// only show the status bar's cancel button while in edit mode
	mySearchBar.showsCancelButton = YES;
	
	// flush and save the current list content in case the user cancels the search later
	[savedContent removeAllObjects];
	[savedContent addObjectsFromArray: filteredListContent];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
#pragma unused (searchBar)
    
	mySearchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
#pragma unused (searchBar)
    
	[filteredListContent removeAllObjects];	// clear the filtered array first
	
	// Search the table content for note text that contains "searchText", ignoring case.
	//	If found add to the mutable array and force the table to reload.
	//
	Note *note;
	NSNumber *val;	

	for (val in listContent)
	{
		int index = [val intValue];
		note = [model getNoteForIndex:index];
		NSRange resultRange = [note.noteText rangeOfString:searchText options:NSCaseInsensitiveSearch];

		// If text matched add the note to the filtered list
		if (resultRange.location != NSNotFound)
		{
			[filteredListContent addObject:val];
		}
	}
	
	[myTableView reloadData];
}

// called when cancel button pressed
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
	// if a valid search was entered but the user wanted to cancel, bring back the saved list content
	if (searchBar.text.length > 0)
	{
		[filteredListContent removeAllObjects];
		[filteredListContent addObjectsFromArray: savedContent];
	}
	
	[myTableView reloadData];
	
	[searchBar resignFirstResponder];
	searchBar.text = @"";
}

// called when Search (in our case "Done") button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
	[searchBar resignFirstResponder];
}

@end

