//
//  BackupPickerController.m
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying the Notepad backups
//
//  Created by Lewis Garrett on 3/5/10.
//  Copyright 2010 Iota. All rights reserved.
//

#import "TopXNotesAppDelegate.h"                                                //leg20210420 - TopXNotes2
#import "Formatter.h"
#import "Constants.h"
#import "Model.h"
#import "BackupPickerController.h"
#import "BackupSettingsViewController.h"                                        //leg20210420 - TopXNotes2


@implementation BackupPickerController

@synthesize backupDescriptionsArray;
@synthesize pickerView;
//@synthesize label;
@synthesize model;
@synthesize restoreButton;
@synthesize removeButton;                                                       //leg20210518 - TopXNotes2

- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}

- (NSString*)pathToBackupData {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:kBackupModelFileName];

	return appFile;
}

// Remove all notes from notepad dialog.                                        //leg20210429 - TopXNotes2
- (void)doEraseNotePad {
    [model removeAllNotes];
    [self alertSimpleAction:@"All Notes Removed!"];

    // Signal that the notepad has been replaced.                               //leg20210705 - TopXNotes 1.6
    TopXNotesAppDelegate *appDelegate =
        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.notepadWasReplaced = YES;
}

-(void)doReplaceNotePad {

	// Reading Defaults 
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict == nil)  
		return;
		
	// Get auto backup notepad data
	NSArray *array = [dict objectForKey: kAutoBackupsArray_Key];
	if (array == nil)
		return;

	NSInteger row = [pickerView selectedRowInComponent:0];
	NSString *backupFileName = [array objectAtIndex:row];
	
	// Prepare to retreive backup file descriptions
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error = nil;	
	NSString *pathToAutoBackupDataFile = [self pathToAutoBackup:backupFileName];
	NSString *pathToModelDataFile = [model pathToData];
	NSString *pathToBackupModelDataFile = [self pathToBackupData];

	// Make a backup copy of the notepad
	[fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
	[fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&error];

	Boolean FILE_DOES_NOT_EXIST = ![fileManager fileExistsAtPath:pathToModelDataFile];
	Boolean FILE_WAS_REMOVED = [fileManager removeItemAtPath:pathToModelDataFile error:&error];
	Boolean ERROR_WAS_ZERO  = [error code] == 0;

	// Delete the old Model file and if no errors replace it with the received Model file.
	if ((FILE_DOES_NOT_EXIST || FILE_WAS_REMOVED) && ERROR_WAS_ZERO) {
		
		// Replace the model (current notepad) with the backup notepad
		Boolean FILE_WAS_COPIED  = [fileManager copyItemAtPath:pathToAutoBackupDataFile toPath:pathToModelDataFile error:&error];
		
		// If file copied ok and made the current notepad, report success
		if (FILE_WAS_COPIED && [model loadData]) {		
			NSLog(@"Backup file %@ was restored successfully.", backupFileName);
			[self alertSimpleAction: @"Notepad backup was restored successfully!"];
		} else	
			NSLog(@"Error copying backup file to model - error=\"%@\"", [error localizedDescription]);
	}
	else
		NSLog(@"Error deleting notepad file - error=\"%@\"", [error localizedDescription]);
    
    // Signal that the notepad has been replaced.                               //leg20210705 - TopXNotes 1.6
    TopXNotesAppDelegate *appDelegate =
        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.notepadWasReplaced = YES;
}

-(IBAction)restoreBackupNotepad
{
//	// Give user opportunity to cancel restore.
//	[self alertOKCancelAction: [NSString stringWithFormat:@"Notepad will be replaced with back-up: %@", label.text]];
    
    // Remove deprecated UIAlertView usage and recode dialog asking             //leg20210420 - TopXNotes2
    //  permission to proceed with Restore action.
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Restore Backup Notepad"
                                   message:[NSString stringWithFormat:@"Notepad will be replaced with back-up: %@", self.backup2Restore]
                                   preferredStyle:UIAlertControllerStyleAlert];
//                                    preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        [self doReplaceNotePad];
    }];
    
     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {
         // Do nothing––action cancelled!
     }];

    [alert addAction:defaultAction];
    [alert addAction:cancelAction];

    [self.backupSettingsViewController presentViewController:alert animated:YES completion:nil];
}

// Remove all notes from notepad dialog.                                        //leg20210429 - TopXNotes2
-(IBAction)removeAllNotesFromNotepad
{
    // Give user opportunity to cancel removing all notes.
    //  Ask permission to proceed with removing notes.
    [NSString stringWithFormat:@"Are you sure you want to delete all %ld notes from this notepad? This action is NOT UNDOABLE!",(long)[self.model numberOfNotes]];
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Remove All Notes?"
                                   message:[NSString stringWithFormat:@"Are you sure you want to delete all %ld notes from this notepad? This action is NOT UNDOABLE!",
                                                (long)[self.model numberOfNotes]]
                                   preferredStyle:UIAlertControllerStyleAlert];
//                                    preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        [self doEraseNotePad];
    }];
    
     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {
         // Do nothing––action cancelled!
     }];

    [alert addAction:defaultAction];
    [alert addAction:cancelAction];

    [self.backupSettingsViewController presentViewController:alert animated:YES completion:nil];
}

#pragma mark  UIPickerViewDataSource methods

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
#pragma unused (pickerView)
    
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
#pragma unused (pickerView, component)
    
	//return kNumberOfAutoBackups;
	return [backupDescriptionsArray count];
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
#pragma unused (pickerView, component)
    
	return [backupDescriptionsArray objectAtIndex:row];
}

#pragma mark  UIPickerViewDelegate methods

-(void)pickerView:(UIPickerView *)inPickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
#pragma unused (component)

	// If the user chooses a new row, update the button title accordingly.      //leg20210518 - TopXNotes2
//	label.text = [self pickerView:inPickerView titleForRow:row forComponent:0];
    self.backup2Restore = [self pickerView:inPickerView titleForRow:row forComponent:0];
    NSString *restoreButtonTitle = [NSString stringWithFormat:@"Restore: %@", self.backup2Restore];
    [restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];
    [restoreButton setTitle:restoreButtonTitle forState:UIControlStateNormal];
}

#pragma mark UIAlertView

- (void)alertSimpleAction:(NSString*)alertMessage
{
//	// open an alert with just an OK button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Backup Notepad" message: alertMessage
//							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//	[alert show];

        // Remove deprecated UIActionSheet usage and recode dialog asking           //leg20210420 - TopXNotes2
        //  permission to proceed with Restore action.

        UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Restore Backup Notepad"
                                       message:alertMessage
                                       preferredStyle:UIAlertControllerStyleAlert];
//                                       preferredStyle:UIAlertControllerStyleActionSheet];

        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
           handler:^(UIAlertAction * action) {
            // Do nothing––no action required!
        }];
        
        [alert addAction:defaultAction];

        [self.backupSettingsViewController presentViewController:alert animated:YES completion:nil];
}

// Removed deprecated UIAlertView usage.                                        //leg20210426 - TopXNotes2
//- (void)alertOKCancelAction:(NSString*)alertMessage
//{
//	// open a alert with an OK and cancel button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Restore Backup Notepad" message: alertMessage
//							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Proceed", nil];
//	[alert show];
//}
//
//
//#pragma mark - UIAlertViewDelegate
//
//- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{	
//#pragma unused (actionSheet)
//    
//	// use "buttonIndex" to decide your action
//	//
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		//NSLog(@"Cancel");
//	} else {
//		//NSLog(@"OK");
//		[self doReplaceNotePad];
//	}
//}
//
//#pragma mark UIActionSheet
//
//- (void)dialogOKCancelAction
//{
//	// open a dialog with an OK and cancel button
//	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
//									delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
//	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
//	//[actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
//}
//
//#pragma mark - UIActionSheetDelegate
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//#pragma unused (actionSheet)
//    
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		NSLog(@"Proceed");
//		//[self sendEmail];
//	}
//	else
//	{
//		NSLog(@"Cancel");
//	}
//}

@end
