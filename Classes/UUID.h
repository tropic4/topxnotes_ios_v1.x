//
//  UUID.h
//  TopXNotes
//
//  Created by Lewis E. Garrett on 3/27/12.
//  Copyright (c) 2012 Iota. All rights reserved.
//

@interface NSString (UUID)

+ (NSString *)uuid;

@end
