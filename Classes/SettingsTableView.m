//
//  SettingsTableView.m
//  TopXNotes
//
//  Created by Lewis Garrett on 3/4/10.
//  Copyright 2010 Iota. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210628 - TopxNotes 1.6
//

#import "TopXNotesAppDelegate.h"                                                //leg20210628 - TopXNotes 1.6
#import "SettingsTableView.h"
#import "BackupSettingsViewController.h"
#import "NoteSettingsViewController.h"
#import "AboutViewController.h"
#import "NSMutableString+EmailEncodingExtensions.h"


@implementation SettingsTableView

//  Note that it is necessary to change the celln connections in IB when cells are		//leg20101219 - 1.0.3
//	added, removed, or re-ordered.														//leg20101219 - 1.0.3
//@synthesize model, cell0, cell1, cell2, cell3;		// Add a Notes Settings row		//leg20110415 - 1.0.4
@synthesize model, cell0, cell1, cell2, cell3;	// Add a row for Encryption Settings	//leg20210628 - TopXNotes 1.6

- (void)viewDidLoad {
	
    [super viewDidLoad];

    // Make the navigation bar a darker color so controls stand-out.            //leg20210628 - TopXNotes 1.6
    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];

    // Add some wallpaper
	//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"XPalm.png"]];
	//self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"XPalmWithBorder.png"]];
    //self.view.backgroundColor = [UIColor colorWithWhite:.5 alpha:1];		//gray close to steel

    // The above bg color setting methods no longer work as of Base SDK iOS     //leg20140205 - 1.2.7
    //  7.0.  I found that it was necessary to set the tableView's
    //  BackgroundView to nil in order for it to work in iOS 6.0.
    [self.view setBackgroundColor:[UIColor colorWithWhite:.5 alpha:1]];	//gray close to steel
    [self.tableView setBackgroundView:nil];

    // Navigation Bar needs to be NOT translucent on iOS 6 in order for the     //leg20140205 - 1.2.7
    //  picker views to be offset under the Navigation Bar, while on iOS 7 if
    //  the Navigation Bar is NOT translucent, it is solid black and different
    //  from all the other Navigation Bars.
//    if (IS_OS_7_OR_LATER)
//        self.navigationController.navigationBar.translucent = YES;
    
    // Hook-up to model which is now owned by AppDelegate.                      //leg20210628 - TopXNotes 1.6
    id appDelegate = [[UIApplication sharedApplication] delegate];
    self.model = [(TopXNotesAppDelegate*)appDelegate model];

    modalViewController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
	modalViewController.model = self.model;
	modalViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
}

// Pass information to view controller segued to.                               //leg20210628 - TopXNotes 1.6
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    if ([segue.identifier isEqualToString:@"About_TopXNotes2_Segue"] ) {
    if ([segue.identifier isEqualToString:@"About_TopXNotes_Segue"] ) {         //leg20210628 - TopXNotes 1.6
        // Nothing to pass for About…
//        UINavigationController *navigationController = segue.destinationViewController;
//        AboutViewController *controller = (AboutViewController *)navigationController.topViewController;
//              ---- or ----
//        AboutViewController *controller = (AboutViewController *)segue.destinationViewController;
//    } else if ([segue.identifier isEqualToString:@"Encryption_Preferences_Segue"] ) {
//        // Pass model to EncryptionSettingsViewController view.                 //leg20210419 - TopXNotes2
//        EncryptionSettingsViewController *controller = (EncryptionSettingsViewController *)segue.destinationViewController;
//        controller.model = self.model;
    } else if ([segue.identifier isEqualToString:@"Note_Preferences_Segue"] ) {
        // Pass model to NoteSettingsViewController view.                       //leg20210628 - TopXNotes 1.6
        NoteSettingsViewController *controller = (NoteSettingsViewController *)segue.destinationViewController;
        controller.model = self.model;
    } else if ([segue.identifier isEqualToString:@"Backup_Restore_Notepad_Segue"] ) {
        // Pass model to BackupSettingsViewController view.                     //leg20210628 - TopXNotes 1.6
        BackupSettingsViewController *controller = (BackupSettingsViewController *)segue.destinationViewController;
        controller.model = self.model;
    }
}

// user clicked the "i" button, present a modal UIViewController
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)

	// present as a modal child or overlay view
    [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];  //leg20140205 - 1.2.7
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;


    [super viewDidUnload];
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)

    // Return YES for supported orientations
	
    //return (interfaceOrientation == UIInterfaceOrientationPortrait);
    //return YES;	// portrait and landscape
    return NO;	
}

#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

// Remove the Note Synchronization Settings row									//leg20101219 - 1.0.3
// Add a Notes Settings row                                                     //leg20110415 - 1.0.4
// Add a row for Encryption Settings                                            //leg20130204 - 1.3.0
//    return 5;
	return 4;                                                                   //leg20210628 - TopXNotes 1.6
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

switch (indexPath.row) {
// Remove the Note Synchronization Settings row                                 //leg20101219 - 1.0.3
// Add a Notes Settings row                                                     //leg20110415 - 1.0.4
// Add a row for Encryption Settings                                            //leg20130204 - 1.3.0
        // Move setting textLabel.text to storyboard cell's content view        //leg20210419 - TopXNotes2
        //  label because setting it here left default "Title" text on left
        //  side of textLabel view.
		case 0:
			{
                cell0.textLabel.text = @"Note Preferences";
                cell0.textLabel.font = [UIFont boldSystemFontOfSize:20];        //leg20210628 - TopXNotes 1.6
                cell0.textLabel.textAlignment = NSTextAlignmentCenter;
                cell0.textLabel.adjustsFontSizeToFitWidth = YES;                //leg20120405 - 1.2.0
                cell0.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				return cell0;
			}
			break;
			
// TopXNotes2 setting                                                           //leg20210628 - TopXNotes 1.6
//        case 1:     // Add a row for Encryption Settings                        //leg20130204 - 1.3.0
//            {
//                cell1.textLabel.text = @"Encryption Preferences";
//                cell1.textLabel.font = [UIFont boldSystemFontOfSize:20];        //leg20210628 - TopXNotes 1.6
//                cell1.textLabel.textAlignment = NSTextAlignmentCenter;
//                cell1.textLabel.adjustsFontSizeToFitWidth = YES;
//                cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//                return cell1;
//            }
//            break;
        
//        case 2:
		case 1:                                                                 //leg20210628 - TopXNotes 1.6
			{
                cell1.textLabel.text = @"Back-up/Restore Notepad";
                cell1.textLabel.font = [UIFont boldSystemFontOfSize:20];        //leg20210628 - TopXNotes 1.6
                cell1.textLabel.textAlignment = NSTextAlignmentCenter;
                cell1.textLabel.adjustsFontSizeToFitWidth = YES;                //leg20120405 - 1.2.0
                cell1.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
				return cell1;
			}
			break;
			
//        case 3:
		case 2:                                                                 //leg20210628 - TopXNotes 1.6
			{
                cell2.textLabel.text = @"Contact Tropical Software";
                cell2.textLabel.font = [UIFont boldSystemFontOfSize:20];        //leg20210628 - TopXNotes 1.6
                cell2.textLabel.textAlignment = NSTextAlignmentCenter;
                cell2.textLabel.adjustsFontSizeToFitWidth = YES;                //leg20120405 - 1.2.0
				return cell2;
			}
			break;
			
//        case 4:
		case 3:                                                                 //leg20210628 - TopXNotes 1.6
			{
//                cell4.textLabel.text = @"About TopXNotes2";
                cell3.textLabel.text = [NSString stringWithFormat:@"About %@",
                                        [self infoValueForKey:@"CFBundleDisplayName"]]; //leg20150318 - 1.5.0
                cell3.textLabel.font = [UIFont boldSystemFontOfSize:20];        //leg20210628 - TopXNotes 1.6
                cell3.textLabel.textAlignment = NSTextAlignmentCenter;
                cell3.textLabel.adjustsFontSizeToFitWidth = YES;                //leg20120405 - 1.2.0
				return cell3;
			}
			break;
			
		default:
//            return cell4;
			return cell3;
			break;
	}
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

    // Navigation logic may go here. Create and push another view controller.
	switch (indexPath.row) {
// Remove the Note Synchronization Settings row											//leg20101219 - 1.0.3
// Add a Notes Settings row		//leg20110415 - 1.0.4
// Add a row for Encryption Settings                                            //leg20130204 - 1.3.0
		case 0:
			{
// This is now handled by segue @"Note_Preferences_Segue".                      //leg20210628 - TopXNotes 1.6
//              // Note Preferences view
//				NoteSettingsViewController *noteSettingsViewController = [[NoteSettingsViewController alloc] initWithNibName:@"NoteSettingsViewController" bundle:nil];
//				noteSettingsViewController.model = self.model;
//				[self.navigationController pushViewController:noteSettingsViewController animated:YES];
			}
			break;

// TopXNotes2 setting                                                           //leg20210628 - TopXNotes 1.6
//        case 1:     // Encryption Settings view                                 //leg20130204 - 1.3.0
//            {
//// This is now handled by segue @"Encryption_Preferences_Segue".                //leg20210419 - TopXNotes2
////                // Encryption Preferences view
////                EncryptionSettingsViewController *encryptionSettingsViewController = [[EncryptionSettingsViewController alloc] initWithNibName:@"EncryptionSettingsViewController" bundle:nil];
////                encryptionSettingsViewController.model = self.model;
////                [self.navigationController pushViewController:encryptionSettingsViewController animated:YES];
//            }
//			break;
            
//        case 2:
		case 1:                                                                 //leg20210628 - TopXNotes 1.6
			{
// This is now handled by segue @"Backup_Restore_Notepad_Segue".                //leg20210420 - TopXNotes2
//				// Backup/Restore settings view
//				BackupSettingsViewController *backupSettingsViewController = [[BackupSettingsViewController alloc] initWithNibName:@"BackupSettingsView" bundle:nil];
//				backupSettingsViewController.model = self.model;
//				[self.navigationController pushViewController:backupSettingsViewController animated:YES];
			}
			break;

//        case 3:
		case 2:                                                                 //leg20210628 - TopXNotes 1.6
			{
				// Contact Tropical Software Support
				[self sendEmail];
			}
			break;

//        case 4:
		case 3:                                                                 //leg20210628 - TopXNotes 1.6
			{
// This is now handled by segue @"About_TopXNotes2_Segue".                      //leg20210628 - TopXNotes 1.6
//				// Present About box as a modal child or overlay view
//                [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];  //leg20140205 - 1.2.7
			}
			break;

		default:
			break;
	}
    
    // Clear row selection.                                                     //leg20210628 - TopXNotes 1.6
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/



#pragma mark - Contact Tropical Support 

// Fetch objects from our bundle based on keys in our InfoPlist.strings.        //leg20210628 - TopXNotes 1.6
- (id)infoValueForKey:(NSString*)key
{
	if ([[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key])
		return [[[NSBundle mainBundle] localizedInfoDictionary] objectForKey:key];
	return [[[NSBundle mainBundle] infoDictionary] objectForKey:key];
}

// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
	//Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email
	//NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	//NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	//completeSubject = [completeSubject stringByAppendingString:@"\""];
	NSString *completeSubject = @"Re: Contact Tropical Software Support";
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	//NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	//NSMutableString *body = @" ";
	NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];

    //NSMutableString *body = [NSMutableString stringWithString:@" "];
    NSMutableString *body = [NSMutableString stringWithFormat:@"%@ %@",         //leg20150924 - 1.5.0
                             [self infoValueForKey:@"CFBundleDisplayName"],     //leg20150924 - 1.5.0
                             versionInfo];                                      //leg20150924 - 1.5.0

	// encode the strings for email
	[subject encodeForEmail];
	[body encodeForEmail];

	NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:support@tropic4.com?subject=%@&body=%@", subject, body];
	NSURL *encodedURL = [NSURL URLWithString:emailMsg];
		 
	// Send the assembled message to Mail!
	[[UIApplication sharedApplication] openURL:encodedURL];
}

- (void)sendEmail
{

	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
        
	//Note *note = [model getNoteForIndex:noteIndex];

	// Set up recipients
	NSArray *toRecipients = [NSArray arrayWithObject:@"support@tropic4.com"]; 
	//NSArray *toRecipients = [NSArray arrayWithObject:@"mac@goofyfooter.com"]; 
	//NSArray *ccRecipients = [NSArray arrayWithObjects:@"second@example.com", @"third@example.com", nil]; 
	//NSArray *bccRecipients = [NSArray arrayWithObject:@"fourth@example.com"]; 
	
	[picker setToRecipients:toRecipients];
	//[picker setCcRecipients:ccRecipients];	
	//[picker setBccRecipients:bccRecipients];

	// Assemble the Email - Don't set any recipients, let user do during composition
	NSString *completeSubject = @"Re: Contact Tropical Software Support";
	//NSString *subjectPrefix = @"Re: Contact Tropical Software Support \"";
	//NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	//completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	//NSMutableString *body = [NSMutableString stringWithString:note.noteText];
	//NSMutableString *body = [NSMutableString stringWithString:@""];
	NSString *versionInfo = [self infoValueForKey:@"CFBundleGetInfoString"];
	//NSMutableString *body = [NSMutableString stringWithString:versionInfo];
    //NSMutableString *body = [NSMutableString stringWithString:versionInfo];
    NSMutableString *body = [NSMutableString stringWithFormat:@"%@ %@",         //leg20150924 - 1.5.0
                             [self infoValueForKey:@"CFBundleDisplayName"],
                             versionInfo];

	[picker setSubject:subject];
	[picker setMessageBody:body isHTML:NO];
	
	// Present the composer
    [self presentViewController:picker animated:YES completion:nil];            //leg20140205 - 1.2.7
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{       
#pragma unused (controller, error)

	NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }

        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7
		
		[self alertEmailStatus:resultMessage];
}

#pragma mark - UIAlertController

// Replace deprecated UIAlertView with UIAlertController.                       //leg20210628 - TopXNotes 1.6
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
               handler:^(UIAlertAction * action) {
                // Do nothing––no action required!
            }];
            
            [alert addAction:defaultAction];

            [self presentViewController:alert animated:YES completion:nil];
}

- (void)alertOKAndCancelWithTitle:(NSString*)title message:(NSString*)message   //leg20210628 - TopXNotes 1.6
{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                   message:message
                                   preferredStyle:UIAlertControllerStyleAlert];
//                                    preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Proceed" style:UIAlertActionStyleDefault
       handler:^(UIAlertAction * action) {
        [self sendEmail];
    }];
    
     UIAlertAction* cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
        handler:^(UIAlertAction * action) {
         // Do nothing––action cancelled!
     }];

    [alert addAction:defaultAction];
    [alert addAction:cancelAction];

    [self presentViewController:alert animated:YES completion:nil];
}

//#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
	// open an alert with just an OK button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
//							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
//	[alert show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210628 - TopXNotes 1.6
    [self alertOKWithTitle:@"Email Status" message:alertMessage];}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
//	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
//							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//	[alert show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210628 - TopXNotes 1.6
    [self alertOKAndCancelWithTitle:@"Email" message:alertMessage];
}


//#pragma mark - UIAlertViewDelegate
//
//- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{	
//#pragma unused (actionSheet)
//
//	// use "buttonIndex" to decide your action
//	//
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		//NSLog(@"Cancel");
//	} else {
//		//NSLog(@"OK");
//		[self sendEmail];
//	}
//}
//
//
//#pragma mark - UIActionSheetDelegate
//
//- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//#pragma unused (actionSheet)
//
//	// the user clicked one of the OK/Cancel buttons
//	if (buttonIndex == 0)
//	{
//		[self sendEmail];
//	}
//	else
//	{
//		;		// user tapped "Cancel" button
//	}	
//}

@end

