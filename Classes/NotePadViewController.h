//
//  NotePadViewController.h
//  NotesTopX
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Iota. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@class Model;

@interface NotePadViewController : UITableViewController <UITableViewDelegate,
															UITableViewDataSource,
															UIAlertViewDelegate,
															UIActionSheetDelegate,
															MFMailComposeViewControllerDelegate> {

    NSString *ascendingIndicator;                                               //leg20121121 - 1.2.2
    NSString *descendingIndicator;                                              //leg20121121 - 1.2.2
	NSMutableDictionary *savedSettingsDictionary;
	NSMutableArray		*autoBackupsArray;
    NSNumber			*nextAutoBackupNumber;
    NSInteger			sortType;                                               //leg20121121 - 1.2.2

	IBOutlet Model* model;

	int noteIndex;
}

@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) UIToolbar * toolBar;                              //leg20121017 - 1.2.2

- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (void)sortControlHit:(id)sender;                                              //leg20121017 - 1.2.2
- (void)updateBadgeValue;
- (void)reloadData;                                                             //leg20121108 - 1.2.2

// Alerts
- (void)alertEmailStatus:(NSString*)alertMessage;
- (void)alertOKCancelAction:(NSString*)alertMessage;

// Action Sheet
- (void)dialogOKCancelAction;

-(void)displayComposerSheet;
-(void)launchMailAppOnDevice;

@end
