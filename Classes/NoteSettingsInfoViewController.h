//
//  NoteSettingsInfoViewController.h
//  TopXNotes
//
//  Created by Lewis Garrett on 5/19/11.
//  Copyright 2011 Tropical Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
//#import "NotePaperView.h"                                                       //leg20140205 - 1.2.7

@class Model;

@interface NoteSettingsInfoViewController : UIViewController
{
	IBOutlet UILabel *appName;
	IBOutlet UILabel *copyright;
	IBOutlet Model	 *model;
}

@property (nonatomic, strong) Model *model;
// Obsolete notePaperView.                                                      //leg20210517 - TopXNotes2
//@property (strong, nonatomic) IBOutlet NotePaperView *notePaperView;            //leg20140205 - 1.2.7
@property (strong, nonatomic) IBOutlet UITextView *instructionsText;            //leg20140205 - 1.2.7

- (IBAction)dismissAction:(id)sender;

@end
