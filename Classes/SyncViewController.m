//
//  SyncViewController.m
//  TopXNotes
//
//  Created by Lewis Garrett on 5/16/09.
//  Copyright 2009 Tropical Software. All rights reserved.
//
//
// Convert from .xib to storyboard segues.                                      //leg20210628 - TopxNotes 1.6
//

// Reachability
#import <SystemConfiguration/SystemConfiguration.h>
#import <Foundation/Foundation.h>
#import <sys/socket.h>
#import <netinet/in.h>
#import <netinet6/in6.h>
#import <arpa/inet.h>
#import <ifaddrs.h>
#import <netdb.h>

#import "TopXNotesAppDelegate.h"
#import "SyncNotePad.h"
//#import "NotesNavigationController.h"
#import "NotePadViewController.h"
#import "SyncViewController.h"
#import "Model.h"
#import "Note.h"
#import "Constants.h"

//INTERFACES:
@interface SyncViewController ()
- (BOOL)hasActiveWiFiConnection;
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message;           //leg20210628 - TopXNotes 1.6
@end

//CLASS IMPLEMENTATIONS:
@implementation SyncViewController

@synthesize numberOfNotesSyncedLabel;
@synthesize model;
@synthesize notePadViewController;
@synthesize notePaperView;                                                      //leg20140205 - 1.2.7

- (void) _showAlert:(NSString*)title
{
//	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title message:@"Check your networking configuration." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alertView show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210628 - TopXNotes 1.6
    [self alertOKWithTitle:title message:@"Check your networking configuration."];
}

- (void) _syncCompleteAlert {
//	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Sync" message:@"Synchronization of notepads complete!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//	[alertView show];
    // Replace deprecated UIAlertView with UIAlertController.                   //leg20210628 - TopXNotes 1.62
    [self alertOKWithTitle:@"Sync" message:@"Synchronization of notepads complete!"];
}

// Replace deprecated UIAlertView with UIAlertController.                       //leg20210628 - TopXNotes 1.6
- (void)alertOKWithTitle:(NSString*)title message:(NSString*)message {
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:title
                                           message:message
                                           preferredStyle:UIAlertControllerStyleAlert];

            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
               handler:^(UIAlertAction * action) {
                // Do nothing––no action required!
            }];
            
            [alert addAction:defaultAction];

            [self presentViewController:alert animated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Make the navigation bar a darker color so controls stand-out.            //leg20210628 - TopXNotes 1.6
    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];

    // Hook-up to model which is now owned by AppDelegate.                      //leg20210628 - TopXNotes 1.6
    id appDelegate = [[UIApplication sharedApplication] delegate];
    self.model = [(TopXNotesAppDelegate*)appDelegate model];

    // Maybe make showing notebook paper background an option in iOS 7.         //leg20140212 - 1.2.7
    if (YES) {
        self.notePaperView.hidden = YES;
        self.view.backgroundColor = [UIColor whiteColor];
    }

// TODO: Defer hooking-up to notePadViewController until necessary.             //leg20210628 - TopXNotes 1.6
//    // Get addressablity back to notepad view so that we can refresh it when we need to
//	NotesNavigationController* notesNavigationController = (NotesNavigationController*)[self parentViewController];
//	notePadViewController = notesNavigationController.notePadViewController;

	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
	
    NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
    if (dict != nil) 
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
        savedSettingsDictionary = [NSMutableDictionary dictionary];

	// Get Sync settings and set the switch's state.
	syncEnabled = [[savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key] boolValue];
	synchSwitch.on = syncEnabled;
	if (syncEnabled) 
		[shortStatusLabel setText:@"Syncing is ON!"];
	else
		[shortStatusLabel setText:@"Syncing is OFF!"];

// This is now handled by segue @"Sync_Info_Segue".                             //leg20210628 - TopXNotes 1.6
//    // Prepare our Info button controller
//	modalViewController = [[SyncInfoViewController alloc] initWithNibName:@"SyncInfoViewController" bundle:nil];
//	modalViewController.model = self.model;
//	modalViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
	
	// add our custom right button to show our modal view controller
	UIButton* modalViewButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
	[modalViewButton addTarget:self action:@selector(modalViewAction:) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *modalButton = [[UIBarButtonItem alloc] initWithCustomView:modalViewButton];
	self.navigationItem.rightBarButtonItem = modalButton;
                                                          //leg20140212 - 1.2.7
    
    // The lined notepaper is displayed only if on < iOS 7.                     //leg20140205 - 1.2.7
    if (!IS_OS_7_OR_LATER) {
        // Adjust lined notebook paper background.
        self.notePaperView.frame = CGRectMake(self.notePaperView.frame.origin.x,
                                              self.notePaperView.frame.origin.y + 4.0,
                                              self.notePaperView.frame.size.width,
                                              self.notePaperView.frame.size.height);
    }

    // Insure lineWidth is not 0.
    self.notePaperView.lineWidth = shortStatusLabel.font.lineHeight;
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)
    
    [super viewWillAppear:animated];

	// Display the number of notes in the notepad
	[numberOfNotesSyncedLabel setText: [NSString stringWithFormat: @"This notepad now contains %@ notes.", [NSNumber numberWithInt: [model numberOfNotes]]]];
	[numberOfNotesSyncedLabel setHidden:NO];
    
    // The lined notepaper is displayed only if on < iOS 7.                     //leg20140205 - 1.2.7
    if (!IS_OS_7_OR_LATER) {
        // Draw lined notebook paper background.
        self.notePaperView.lineWidth = shortStatusLabel.font.lineHeight;
        [self.view setNeedsDisplay];
        [self.notePaperView setNeedsDisplay];
    }
}

// user clicked the "i" button, present a modal UIViewController
- (IBAction)modalViewAction:(id)sender
{
#pragma unused (sender)
// This is now handled by segue @"Sync_Info_Segue".                             //leg20210628 - TopXNotes 1.6
//	// present as a modal child or overlay view
//    [[self navigationController] presentViewController:modalViewController animated:YES completion:nil];    //leg20140205 - 1.2.7
    [self performSegueWithIdentifier:@"Sync_Info_Segue" sender: self];
}

- (void)viewWillDisappear:(BOOL)animated
{
#pragma unused (animated)

    [super viewWillDisappear:animated];

}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)
    // Return YES for supported orientations
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}



// If we display an error or an alert that the remote disconnected, handle dismissal and return to setup
- (void) alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (buttonIndex, alertView)

}

#pragma mark Enable/Disable Sync Connection
- (IBAction)syncEnableOrDisable:(id)sender { 
 	//set a NSTimer or something
	//	[self _showAlert:@"Checking WIFI connection..."];
	if (![self hasActiveWiFiConnection] && synchSwitch.on) {
		[self _showAlert:@"A WIFI connection is required to Sync TopXNotes -- Turn WIFI On in Settings!"];
		[shortStatusLabel setText:@"Syncing is OFF."];
		synchSwitch.on = NO;
		return;
	}
	
	// Start or Stop the network connection depending.
	id appDelegate = [[UIApplication sharedApplication] delegate];
	SyncNotePad *syncNotePad = [(TopXNotesAppDelegate*)appDelegate syncNotePad];
	[(SyncNotePad*)syncNotePad toggleSyncing:[sender isOn]];
	if ([sender isOn]) 
		[shortStatusLabel setText:@"Syncing is ON!"];
	else
		[shortStatusLabel setText:@"Syncing is OFF!"];

	// Save Sync setting.														
	syncEnabled = [sender isOn];																					
	[savedSettingsDictionary setObject:[NSNumber numberWithBool:[sender isOn]] forKey:kSyncEnabledOrDisabled_Key];
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (BOOL)hasActiveWiFiConnection
{
	// An enumeration that defines the return values of the network state of the device.
	typedef enum {
		NotReachable = 0,
		ReachableViaCarrierDataNetwork,
		ReachableViaWiFiNetwork
	} NetworkStatus;

	 SCNetworkReachabilityFlags     flags;
	 SCNetworkReachabilityRef		reachabilityRef;
	 BOOL							gotFlags;
	 
	 reachabilityRef   = SCNetworkReachabilityCreateWithName(CFAllocatorGetDefault (), [@"www.google.com"UTF8String]);
	 gotFlags          = SCNetworkReachabilityGetFlags(reachabilityRef, &flags);
	 CFRelease(reachabilityRef);
	 if (!gotFlags) {
		return NO;
	 }
	 
	//if( flags & ReachableDirectWWAN ) {
	//	return NO;
	//}
	 
	if( flags & ReachableViaWiFiNetwork ) {
		return YES;
	}

	return NO;
}

@end
