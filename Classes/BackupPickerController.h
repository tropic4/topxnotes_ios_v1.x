//
//  BackupPickerController.h
//  TopXNotes
//
// Abstract: Controller to managed a picker view displaying imperial weights.
//
//  Created by Lewis Garrett on 3/5/10.
//  Copyright 2010 Iota. All rights reserved.
//

@class Model;
@class BackupSettingsViewController;                                            //leg20210420 - TopXNotes2
@interface BackupPickerController : NSObject <UIPickerViewDataSource, UIPickerViewDelegate,
												UIAlertViewDelegate, UIActionSheetDelegate> {

    IBOutlet UIPickerView	*pickerView;
//    IBOutlet UILabel		*label;
    IBOutlet UIButton		*restoreButton;
    IBOutlet UIButton       *removeButton;                                      //leg20210518 - TopXNotes2
    IBOutlet Model          *model;

	NSMutableArray          *backupDescriptionsArray;
    NSString                *backup2Restore;                                    //leg20210518 - TopXNotes2
}

@property (nonatomic, strong) NSMutableArray *backupDescriptionsArray;
@property (nonatomic, strong) IBOutlet UIPickerView *pickerView;
//@property (nonatomic, strong) IBOutlet UILabel *label;
@property (nonatomic, strong) IBOutlet UIButton	*restoreButton;
@property (nonatomic, strong) IBOutlet UIButton *removeButton;                  //leg20210518 - TopXNotes2
@property (nonatomic, strong) Model *model;
@property (nonatomic, strong) BackupSettingsViewController *backupSettingsViewController;   //leg20210420 - TopXNotes2
@property (nonatomic, strong) NSString *backup2Restore;                         //leg20210518 - TopXNotes2

- (IBAction)restoreBackupNotepad;
- (void)doEraseNotePad;                                                         //leg20210429 - TopXNotes2
- (void)doReplaceNotePad;
- (void)alertSimpleAction:(NSString*)alertMessage;
//- (void)alertOKCancelAction:(NSString*)alertMessage;
- (NSString*)pathToAutoBackup:(NSString*)withFileName;
- (NSString*)pathToBackupData;

@end
