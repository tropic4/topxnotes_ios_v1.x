//
//  NotePadViewController.m
//  NotesTopX
//
//  Created by Lewis Garrett on 4/11/09.
//  Copyright 2009 Iota. All rights reserved.
//
// Convert from .xib to storyboard segues.                                      //leg20210626 - TopxNotes 1.6
//

#import "TopXNotesAppDelegate.h"                                                //leg20210626 - TopxNotes 1.6
#import "NotePadViewController.h"
#import "NoteViewController.h"
#import "Formatter.h"
#import "Constants.h"
#import "Model.h"
#import "Note.h"
#import "NoteListCell.h"
#import "NSMutableString+EmailEncodingExtensions.h"

NotePadViewController* gNotePadViewController;

@implementation NotePadViewController
@synthesize model;
@synthesize toolBar;  //leg20121017 - 1.2.2

// Unwind back to notepad list.                                                 //leg20210705 - TopXNotes 1.6
- (IBAction)unwind2NotePad:(UIStoryboardSegue *)segue {}

// Reload the note list table, properly sorted          //leg20121022 - 1.2.2
- (void)reloadData
{
    switch (sortType) {
        case NotesSortByDateAscending:
            [model sortNotesByDate:YES];	// sort date ascending
            break;
            
        case NotesSortByDateDescending:
            [model sortNotesByDate:NO];		// sort date descending
            break;
            
        case NotesSortByTitleAscending:
            [model sortNotesByTitle:YES];	// sort title ascending
            break;
            
        case NotesSortByTitleDescending:
            [model sortNotesByTitle:NO];	// sort title descending
            break;
            
        default:
            break;
    }
    [self.tableView reloadData];
}

// Segue to create a new note.                                                  //leg20210414 - TopXNotes2
-(IBAction)newNote {
    // Since .xibs have been converted to Storyboard so must now use segues.    //leg20210414 - TopXNotes2
    [self performSegueWithIdentifier:@"New_Note_Segue" sender: self];
}

// Pass information to view controller segued to.                               //leg20210414 - TopXNotes2
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"New_Note_Segue"] || [segue.identifier isEqualToString:@"Edit_Note_Segue"]) {
        NoteViewController *controller = (NoteViewController *)segue.destinationViewController;
        controller.model = self.model;
        if ([segue.identifier isEqualToString:@"New_Note_Segue"]) {
            controller.noteIndex = -1;
        } else {
            controller.noteIndex = noteIndex;
        }
        
//        // Restore < Back navigaton button.                                     //leg20210526 - TopXNotes2
//        controller.navigationItem.leftBarButtonItem = self.splitViewController.displayModeButtonItem;
//        controller.navigationItem.leftItemsSupplementBackButton = YES;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
	
    // Set Appearance Style for view.                                           //leg20210626 - TopxNotes 1.6
    if (@available(iOS 13.0, *)) {
        self.overrideUserInterfaceStyle = UIUserInterfaceStyleLight;
    }
    
    // Make the tab bar a darker color so selected tab stands-out.              //leg20210626 - TopxNotes 1.6
    self.tabBarController.tabBar.barTintColor = [UIColor lightGrayColor];

    // Make the navigation bar a darker color so controls stand-out.            //leg20210626 - TopxNotes 1.6
    self.navigationController.navigationBar.barTintColor = [UIColor lightGrayColor];

    // Watch for refresh notelist notifications.                                //leg20210626 - TopXNotes2
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadData) name:kNotification_Refresh_NoteList object:nil];

    // Hook-up to model which is now owned by AppDelegate.                      //leg20210626 - TopxNotes 1.6
    TopXNotesAppDelegate *appDelegate =
        (TopXNotesAppDelegate*)[[UIApplication sharedApplication] delegate];
    self.model = appDelegate.model;

    // Establish which tab we are in.                                           //leg20210626 - TopxNotes 1.6
    NSString *tabTitle = self.navigationItem.title;
//    //NSComparisonResult compareResult = [tabTitle compare:@"TopXNotes"];
//    NSRange range = NSMakeRange(0,9);
//    NSComparisonResult compareResult = [tabTitle compare:@"TopXNotes"
//                                                 options:NSCaseInsensitiveSearch
//                                                   range:range];

    // Establish Edit button as left button of navigation bar
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    
    //Add newNote button only to TopXNotes… tab.                                leg20210416 - TopXNotes2
//    if (compareResult == NSOrderedSame) {
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes 1.6
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc ]
                                                  initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                                  target: self action:@selector(newNote)];
    }

    // Log information about the device and make sure the model gets saved.     //leg20120313 - 1.2.0
    // Reading Defaults                                                         //leg20121017 - 1.2.2
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
    
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
	// Get current note sort type
    NSNumber *sortTypeNumber;
    if (!(sortTypeNumber = [savedSettingsDictionary objectForKey:kSortType_Key])) {
        // Set default sort type.
        sortTypeNumber = [NSNumber numberWithInt:kDefaultSortType];
    }
    
    [savedSettingsDictionary setObject:sortTypeNumber forKey:kSortType_Key];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Set sort type
	sortType = [[savedSettingsDictionary objectForKey:kSortType_Key] intValue];
	
        
// Change the way sort controls are displayed.  Through 1.2.6 the controls      //leg20140205 - 1.2.7
//  were placed in a toolbar inside the navigation bar's title.  Beginning
//  iOS 7, this display became quite ugly.  Therefore, changed to using
//  the NavigationController's inherent toolbar to display the sort
//  controls as text buttons.

    // Add ability to sort note list by Title or Date.                          //leg20121017 - 1.2.2
    
    // Button to control type of sort.
    UIBarButtonItem * sortFieldBarButtonItem = [[UIBarButtonItem alloc]
                                                initWithTitle:@"Sort by Date" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
    [sortFieldBarButtonItem setTag:1];
    
    // Button to control direction of sort.
    UIBarButtonItem * sortDirectionBarButtonItem = [[UIBarButtonItem alloc]
                                                initWithTitle:@"Ascending" style:UIBarButtonItemStyleBordered target:self action:@selector(sortControlHit:)];
    [sortDirectionBarButtonItem setTag:2];
    
    // Spacer button
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    self.navigationController.toolbarHidden=NO;
    self.navigationController.toolbar.barStyle = UIBarStyleBlack;
    self.navigationController.toolbar.translucent = YES;
//    if (IS_OS_7_OR_LATER)                                                       //leg20140205 - 1.2.7
//        self.navigationItem.title = @"";    // Blank title area for iOS 7.
   
    self.toolbarItems = [NSArray arrayWithObjects:flexiableItem, sortFieldBarButtonItem, flexiableItem, sortDirectionBarButtonItem, flexiableItem, nil];
    
    
    switch (sortType) {
        case NotesSortByDateAscending:
//            [sortFieldBarButtonItem setTitle:@"Sort by Date"];
            [sortFieldBarButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Ascending"];
            break;
            
        case NotesSortByDateDescending:
//            [sortFieldBarButtonItem setTitle:@"Sort by Date"];
            [sortFieldBarButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Descending"];
            break;
            
        case NotesSortByTitleAscending:
//            [sortFieldBarButtonItem setTitle:@"Sort by Title"];
            [sortFieldBarButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Ascending"];
            break;
            
        case NotesSortByTitleDescending:
//            [sortFieldBarButtonItem setTitle:@"Sort by Title"];
            [sortFieldBarButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
            [sortDirectionBarButtonItem setTitle:@"Descending"];
            break;
            
        default:
            break;
    }
    	
	// If we are in "TopXNotes" tab, do the auto backup process
//	if (compareResult == NSOrderedSame) {
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes 1.6

		// Get iPhone device information
		UIDevice *device = [UIDevice currentDevice];
        //NSString *uniqueIdentifier = [device uniqueIdentifier];

        // Use our generated unique identifier as a replacement for the deprecated  //leg20130514 - 1.2.6
		//  [device uniqueIdentifier].
//        NSUserDefaults *defaults;
//		defaults = [NSUserDefaults standardUserDefaults];
//		NSString *uniqueIdentifier = [defaults objectForKey:kUUID_GENERATED_KEY];
        NSString *uniqueIdentifier = [model deviceUDID];                    //leg20140116 - 1.2.6
        
		NSString *name = [device name];
		NSString *systemName = [device systemName];
		NSString *localizedModel = [device localizedModel];
		NSString *deviceModel = [device model];
		NSString *systemVersion = [device systemVersion];
		NSLog(@"Device UDID: %@, Model: %@, Localized Model: %@, Name: %@", uniqueIdentifier, deviceModel, localizedModel, name);
		NSLog(@"System Name: %@, System Version: %@", systemName, systemVersion);
        
// UDID is now set in the TopXNotesAppDelegate.                                 //leg20140116 - 1.2.6
//		// Set the device UDID (used for identifying which iDevice is syncing with Mac)
//		[model setDeviceUDID:uniqueIdentifier];
//		
//		// Set the real device UDID so that when Apple removes the deprecated: [device uniqueIdentifier] that
//      //  we will be able to convert it to the generated UDID in the Mac datafile.
//		[model setRealDeviceUDID:uniqueIdentifier];
        
        // Fix notepad by clearing nil objects                                  //leg20131204 - 1.2.6
        if ([[model notePadVersion] integerValue] <= kNotePadVersion3) {
            for (int i=0; i < [model numberOfNotes]; i++) {                     //leg20130314 - 1.2.3
                Note *note = [model	getNoteForIndex:i];
//                BOOL noteChanged = NO;
                if (note.noteID == nil) {
                    note.noteID = [NSNumber numberWithUnsignedLong:0];
//                    noteChanged = YES;
                }
                if (note.groupID == nil) {
                    note.groupID = [NSNumber numberWithUnsignedLong:0];
//                    noteChanged = YES;
                }
                if (note.syncFlag == nil) {
                    note.syncFlag = [NSNumber numberWithBool:NO];
//                    noteChanged = YES;
                }
                if (note.needsSyncFlag == nil) {
                    note.needsSyncFlag = [NSNumber numberWithBool:NO];
//                    noteChanged = YES;
                }
                [model replaceNoteAtIndex:i withNote:note];
            }
        }

		// Must save model here in case this was a new notepad with no notes in it yet, else there
        //  is no representation on disk which screws Sync up.
		[model saveData];					// Insure deviceUDID is saved	
    }
    
    [self reloadData];      //leg20121022 - 1.2.2
}

- (void)viewWillAppear:(BOOL)animated {
#pragma unused (animated)

    [super viewWillAppear:animated];
    
	// Refresh note list
	[self.tableView reloadData];
	
    // Fix and Restore Notes tab badge value update                             //leg20210708 - TopxNotes 1.6
	// Set Notes tab badge value to the number of un-synced notes.				//leg20110421 - 1.0.4
//	 // Remove for now the badge value update from Notes tab.					//leg20110523 - 1.0.4
    [self updateBadgeValue];

// Moved this code from -viewDidLoad.  The code would not be executed after the	//leg20110614 - 1.1.0
//	initial start-up of the App on a device that background/resumes Apps.  This
//	insures that backups are taken when the notepad changes.

	// Check to see if we are in the "TopXNotes" tab.
	//	This check is necessary to keep from doing the auto backup process a
	//	second time after startup.  This is because NotePadView is reused
	//	when the "Email" tab is selected and so viewDidLoad can be entered
	//	a second time.  
	NSString *tabTitle = self.navigationItem.title;
//    //NSComparisonResult compareResult = [tabTitle compare:@"TopXNotes"];
//    NSRange range = NSMakeRange(0,9);                                           //leg20150924 - 1.5.0
//    NSComparisonResult compareResult =                                          //leg20150924 - 1.5.0
//                        [tabTitle compare:@"TopXNotes" options:nil range:range];//leg20150924 - 1.5.0
	
	// If we are in "TopXNotes" tab, do the auto backup process
//	if (compareResult == NSOrderedSame) {
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes 1.6
	    //noteText.font = [UIFont fontWithName:kNoteTextFont size:kNoteTextFontSize];

		// Reading Defaults 
		NSUserDefaults *defaults;
		defaults = [NSUserDefaults standardUserDefaults];
		
		NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
		if (dict != nil) 
			savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
		else
			savedSettingsDictionary = [NSMutableDictionary dictionary];

		// Get Font defaults																//leg20110416 - 1.0.4
		NSString * fontName = [savedSettingsDictionary objectForKey: kNoteFontName_Key];
		if (fontName == nil) {
			fontName = kNoteTextFont;
			[savedSettingsDictionary setObject:fontName forKey:kNoteFontName_Key];
		}
		NSString * fontSize = [savedSettingsDictionary objectForKey: kNoteFontSize_Key];
		if (fontSize == nil) {
			fontSize = [NSString stringWithFormat:@"%d", kNoteTextFontSize];
			[savedSettingsDictionary setObject:fontSize forKey:kNoteFontSize_Key];
		}

		// Get Sync settings.																//leg20110502 - 1.0.4
		NSNumber * syncEnabledOrDisabled = [savedSettingsDictionary objectForKey: kSyncEnabledOrDisabled_Key];
		if (syncEnabledOrDisabled == nil) {
			syncEnabledOrDisabled = [NSNumber numberWithBool:NO];
			[savedSettingsDictionary setObject:syncEnabledOrDisabled forKey:kSyncEnabledOrDisabled_Key];
		}

		// Get auto notepad backup data
		NSMutableArray *array = [dict objectForKey: kAutoBackupsArray_Key];
		if (array != nil) 
			autoBackupsArray = [NSMutableArray arrayWithArray:array];
		else
			autoBackupsArray = [NSMutableArray array];

// Moved this code from -viewDidLoad.                                       //leg20120313 - 1.2.0
//
//		// Get iPhone device information
//		UIDevice *device = [UIDevice currentDevice];
//		NSString *uniqueIdentifier = [device uniqueIdentifier];
//		NSString *name = [device name];
//		NSString *systemName = [device systemName];
//		NSString *localizedModel = [device localizedModel];
//		NSString *deviceModel = [device model];
//		NSString *systemVersion = [device systemVersion];
//		NSLog(@"Device UDID: %@, Model: %@, Localized Model: %@, Name: %@", uniqueIdentifier, deviceModel, localizedModel, name);
//		NSLog(@"System Name: %@, System Version: %@", systemName, systemVersion);
//
//		// Set the device UDID (used for identifying which iDevice is syncing with Mac)
//		[model setDeviceUDID:uniqueIdentifier];
//		
//		// Stop saving the model here.  It was causing backup of notepad every time -viewWillAppear		//leg20110614 - 1.1.0
//		//  even though no notes had changed.															
//		//[model saveData];					// Insure deviceUDID is saved								
		
		// Initialize the next backup number
		nextAutoBackupNumber = [dict objectForKey: kNextAutoBackupNumber_Key];
		NSInteger nextBackupNumber = 0;
		if (nextAutoBackupNumber == nil)  {
			nextBackupNumber = 1;			// first run
		} else { 
			nextBackupNumber = [nextAutoBackupNumber intValue];
		}
		
		// Prepare to copy notepad
		NSFileManager *fileManager = [NSFileManager defaultManager];
		NSError *error = nil;
		NSString *pathToModelDataFile = [model pathToData];
		NSString *nextBackupFileName = [NSString stringWithFormat:@"%ld_%@", (long)nextBackupNumber, kBackupModelFileName];
		NSString *pathToBackupModelDataFile = [self pathToAutoBackup:nextBackupFileName];
		NSInteger lastBackupIndex;
		NSString *lastBackupFileName;										
		NSString *pathToLastBackupModelDataFile;
		NSDictionary *fileAttributesDictionary;
		NSDate *nextBackupModificationDate;
		NSDate *lastBackupModificationDate;
		BOOL result = NO;
		
		if ([autoBackupsArray count] > 0) {
			lastBackupIndex  = [autoBackupsArray count] < kNumberOfAutoBackups ? [autoBackupsArray count]-1 : kNumberOfAutoBackups-1;
			lastBackupFileName = [autoBackupsArray objectAtIndex:lastBackupIndex];										
			pathToLastBackupModelDataFile = [self pathToAutoBackup:lastBackupFileName];

			// Get the modification date of the current notepad (would be next backup)
			fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToModelDataFile error:&error];
			nextBackupModificationDate = [fileAttributesDictionary fileModificationDate];

			// Get the modification date of the last backup
			fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToLastBackupModelDataFile error:&error];
			lastBackupModificationDate = [fileAttributesDictionary fileModificationDate];
			
			// if the modification date of the last backup matches that of the current notepad, no need to back it up 
			if ([nextBackupModificationDate isEqualToDate:lastBackupModificationDate])
				return;
		}

		// Make a backup copy of the notepad
		[fileManager removeItemAtPath:pathToBackupModelDataFile error:NULL];
		[fileManager copyItemAtPath:pathToModelDataFile toPath:pathToBackupModelDataFile error:&error];
		if ([error code] != 0) {
			NSLog(@"Error making a backup of notepad file! Error code=%ld", (long)[error code]);
			return;
		}
		
		// Set the modification date of the backed-up notepad to the same as the current notepad model.		//leg20110614 - 1.1.0
		fileAttributesDictionary = [fileManager attributesOfItemAtPath:pathToModelDataFile error:&error];		//leg20110629 - 1.1.0
		nextBackupModificationDate = [fileAttributesDictionary fileModificationDate];							//leg20110629 - 1.1.0
		fileAttributesDictionary = [NSDictionary dictionaryWithObject:nextBackupModificationDate forKey:NSFileModificationDate];
		result = [fileManager setAttributes:fileAttributesDictionary ofItemAtPath:pathToBackupModelDataFile error:&error];
		if ([error code] != 0 || !result) {
			NSLog(@"Error setting modification date of backup notepad file! Error code=%ld", (long)[error code]);
			return;
		}

		// Add the backup to the list of backups
		[autoBackupsArray addObject:nextBackupFileName];

		// If backups list is not full yet, just increment to next backup number
		if ([autoBackupsArray count] < kNumberOfAutoBackups) {
			nextAutoBackupNumber = [NSNumber numberWithInteger:++nextBackupNumber];
		} else {
		
			// Backups list is full, reset backup number and get rid of oldest backup
			if (nextBackupNumber < kNumberOfAutoBackups) {
				nextAutoBackupNumber = [NSNumber numberWithInteger:++nextBackupNumber];
			} else {
			
				// start back with 1st backup number
				nextAutoBackupNumber = [NSNumber numberWithInt:1];		
			}
			
			// remove oldest backup
			if ([autoBackupsArray count] > kNumberOfAutoBackups)
				[autoBackupsArray removeObjectAtIndex:(NSUInteger)0];	
		}

		// Update the backup settings
		[savedSettingsDictionary setObject:nextAutoBackupNumber forKey:kNextAutoBackupNumber_Key];
		[savedSettingsDictionary setObject:autoBackupsArray forKey:kAutoBackupsArray_Key];

		// Save settings
		[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
#pragma unused (animated)

    [super viewWillDisappear:animated];
}

#pragma mark Table Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#pragma unused (tableView)

    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#pragma unused (tableView, section)

    return [model numberOfNotes];
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	NoteListCell *cell = (NoteListCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (cell == nil)
	{
//		cell = [[[NoteListCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        cell = [[NoteListCell alloc]
                 initWithReuseIdentifier:CellIdentifier];          //leg20140205 - 1.2.7
	}
		
	int row = (int)indexPath.row;
	Note* note = [model getNoteForIndex: row];

	// Check to see if it is a synced note
	//if ([note.noteID unsignedLongValue] == 0)
    if ([note.needsSyncFlag unsignedLongValue] == 0)                            //leg20130918 - 1.2.6
		cell.dateLabel.textColor = [UIColor blueColor];		// Show it is not a synced note
	else
		cell.dateLabel.textColor = [UIColor redColor];		// Show it is a synced note

    // Display title and modification date.
	NSString* dateString = [NSString longDate:note.date];
	cell.noteTitleLabel.text = note.title;
	cell.dateLabel.text = dateString;
    
//    // Display creation date in 2-line format for testing purposes.
//	NSString* dateString2 = [NSString longDate:note.createDate];
//    cell.infoLabel.text = dateString2;
    
    if (IS_OS_7_OR_LATER)                                                      //leg20140205 - 1.2.7
        cell.selectionStyle = UITableViewCellSelectionStyleDefault;
	
	return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
	forRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)
	
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		int row = (int)indexPath.row;
		[model removeNoteAtIndex:row];
		
		[self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] 
		                      withRowAnimation:UITableViewRowAnimationFade];

        // Fix and Restore Notes tab badge value update                         //leg20210708 - TopxNotes 1.6
		// Insure tab item badge value is updated.								//leg20110421 - 1.0.4
		// Remove for now the badge value update from Notes tab.				//leg20110523 - 1.0.4
        [self updateBadgeValue];
	}
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma unused (tableView)

	int row = (int)indexPath.row;
	Note *note = [model getNoteForIndex:row];


	// If in the Email tab then send the note to Mail,
	//	else, put the note in a NoteView
	NSString *tabTitle = self.navigationItem.title;
	NSComparisonResult compareResult = [tabTitle compare:@"Email"];

	if (compareResult == NSOrderedSame) {
		noteIndex = row;
		
		// The response to this dialog is processed in alertOKCancelAction delegate -alertView (sendEmail)
		UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:[NSString stringWithFormat: @"Email note \"%@\"?", note.title]
				delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
		actionSheet.actionSheetStyle = UIActionSheetStyleDefault;

		// Show the dialog coming out of the tab bar		
		UITabBar *tabBar = self.tabBarController.tabBar;
		[actionSheet showFromTabBar:tabBar];
		
//    } else {
//		NoteViewController *controller = [[NoteViewController alloc]
//										   initWithNibName:@"NoteView" noteIndex:row];
//
//		controller.model = self.model;
//
//		[self.navigationController pushViewController:controller animated:YES];
//	}
    } else {
        // Make the note edit view the current view.                            //leg20210626 - TopxNotes 1.6
        
        // Since .xibs have been converted to Storyboard so must now use segues.
        noteIndex = row;
        [self performSegueWithIdentifier:@"Edit_Note_Segue" sender: self];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
#pragma unused (interfaceOrientation)

    // Return YES for supported orientations
	
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
NSLog(@"NotePadViewController didReceiveMemoryWarning");
}


#pragma mark Private Methods

// Sort type changed.                      //leg20121022 - 1.2.2

// Change the way sort controls are displayed.  Through 1.2.6 the controls      //leg20140205 - 1.2.7
//  were placed in a toolbar inside the navigation bar's title.  Beginning
//  iOS 7, this display became quite ugly.  Therefore, changed to using
//  the NavigationController's inherent toolbar to display the sort
//  controls as text buttons.

- (void)sortControlHit:(id)sender {
    
    UIBarButtonItem * sortButtonItem = sender;
    
    switch (sortButtonItem.tag) {
            
        case 1:     // sort field
        {
            switch (sortType) {
                case NotesSortByDateAscending:
                    sortType = NotesSortByTitleAscending;
//                    [sortButtonItem setTitle:@"Sort by Title"];
                    [sortButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
                    break;
                    
                case NotesSortByDateDescending:
                    sortType = NotesSortByTitleDescending;
//                    [sortButtonItem setTitle:@"Sort by Title"];
                    [sortButtonItem setTitle:@"Sorted by Title"];               //leg20150924 - 1.5.0
                    break;
                    
                case NotesSortByTitleAscending:
                    sortType = NotesSortByDateAscending;
//                    [sortButtonItem setTitle:@"Sort by Date"];
                    [sortButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
                    break;
                    
                case NotesSortByTitleDescending:
                    sortType = NotesSortByDateDescending;
//                    [sortButtonItem setTitle:@"Sort by Date"];
                    [sortButtonItem setTitle:@"Sorted by Date"];                //leg20150924 - 1.5.0
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        case 2:
        {
            switch (sortType) {
                case NotesSortByDateAscending:
                    sortType = NotesSortByDateDescending;
                    [sortButtonItem setTitle:@"Descending"];
                    break;
                    
                case NotesSortByDateDescending:
                    sortType = NotesSortByDateAscending;
                    [sortButtonItem setTitle:@"Ascending"];
                    break;
                    
                case NotesSortByTitleAscending:
                    sortType = NotesSortByTitleDescending;
                    [sortButtonItem setTitle:@"Descending"];
                    break;
                    
                case NotesSortByTitleDescending:
                    sortType = NotesSortByTitleAscending;
                    [sortButtonItem setTitle:@"Ascending"];
                    break;
                    
                default:
                    break;
            }
        }
            break;
            
        default:
            break;
    }
    
    
	// Reading Defaults
	NSUserDefaults *defaults;
	defaults = [NSUserDefaults standardUserDefaults];
    
	NSDictionary* dict = [defaults objectForKey: kRestoreDataDictionaryKey];
	if (dict != nil)
		savedSettingsDictionary = [NSMutableDictionary dictionaryWithDictionary:dict];
	else
		savedSettingsDictionary = [NSMutableDictionary dictionary];
    
	// Update the Font settings in user defaults
	[savedSettingsDictionary setObject:[NSNumber numberWithInteger:sortType] forKey:kSortType_Key];
	
	// Save settings
	[[NSUserDefaults standardUserDefaults] setObject:savedSettingsDictionary forKey:kRestoreDataDictionaryKey];
	[[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self reloadData];
}

// Fix and Restore Notes tab badge value update                                 //leg20210708 - TopxNotes 1.6
// Update the tab bar item badge value.
- (void)updateBadgeValue {
    NSString *tabTitle = self.navigationItem.title;
//    NSRange range = NSMakeRange(0,9);                                           //leg20150924 - 1.5.0
//    NSComparisonResult compareResult =                                          //leg20150924 - 1.5.0
//                        [tabTitle compare:@"TopXNotes" options:nil range:range];//leg20150924 - 1.5.0
//
//    // If we are in "TopXNotes" tab, set the badge.
//    if (compareResult == NSOrderedSame) {
    
    // If we are in "TopXNotes" tab, set the badge.                             //leg20210708 - TopxNotes 1.6
//    if ([tabTitle hasSuffix:@"TopXNotes"]) {
    if ([tabTitle hasPrefix:@"TopXNotes"]) {                                    //leg20210713 - TopXNotes2
        int unSyncedNotesCount = 0;
        for (int i=0; i < [model numberOfNotes]; i++) {
            Note *note = [model    getNoteForIndex:i];
//            if ([note.noteID unsignedLongValue] == 0)
            if ([note.needsSyncFlag boolValue])                                 //leg20210708 - TopxNotes 1.6
                unSyncedNotesCount++;
        }

        if (unSyncedNotesCount > 0)
            self.tabBarController.tabBar.selectedItem.badgeValue = [NSString stringWithFormat:@"%d", unSyncedNotesCount];   //leg20210713 - TopXNotes2
        else
            self.tabBarController.tabBar.selectedItem.badgeValue = nil;         //leg20210713 - TopXNotes2
    }
}

- (NSString*)pathToAutoBackup:(NSString*)withFileName {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	
    if (!documentsDirectory) {
        NSLog(@"Documents directory not found!");
        return @"";  
    }
	
    NSString *appFile = [documentsDirectory stringByAppendingPathComponent:withFileName];

	return appFile;
}


// old way of sending Email before MFMailComposeViewController in iPhone OS 3.0
- (void)launchMailAppOnDevice
{
	Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email
	NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	NSMutableString *body = [NSMutableString stringWithString:note.noteText];

	// encode the strings for email
	[subject encodeForEmail];

	[body replaceOccurrencesOfString:@"\r" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];	// Make sure CRs are recognized
	[body encodeForEmail];

	NSString *emailMsg = [NSMutableString stringWithFormat:@"mailto:?subject=%@&body=%@", subject, body];
	NSURL *encodedURL = [NSURL URLWithString:emailMsg];
		 
	// Send the assembled message to Mail!
	[[UIApplication sharedApplication] openURL:encodedURL];
}


- (void)sendEmail
{

	// This can run on devices running iPhone OS 2.0 or later  
	// The MFMailComposeViewController class is only available in iPhone OS 3.0 or later. 
	// So, we must verify the existence of the above class and provide a workaround for devices running 
	// earlier versions of the iPhone OS. 
	// We display an email composition interface if MFMailComposeViewController exists and the device can send emails.
	// We launch the Mail application on the device, otherwise.
	
	Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
	if (mailClass != nil)
	{
		// We must always check whether the current device is configured for sending emails
		if ([mailClass canSendMail])
		{
			[self displayComposerSheet];
		}
		else
		{
			[self launchMailAppOnDevice];
		}
	}
	else
	{
		[self launchMailAppOnDevice];
	}
}

-(void)displayComposerSheet 
{
	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;
        
	Note *note = [model getNoteForIndex:noteIndex];

	// Assemble the Email - Don't set any recipients, let user do during composition
	NSString *subjectPrefix = @"Re: TopXNotes Note --> \"";
	NSString *completeSubject = [subjectPrefix stringByAppendingString:note.title];
	completeSubject = [completeSubject stringByAppendingString:@"\""];
	
	NSMutableString *subject = [NSMutableString stringWithString:completeSubject];
	NSMutableString *body = [NSMutableString stringWithString:note.noteText];
    
    // Modified body preparation to match procedure used in NoteViewController  //leg20150924 - 1.5.0
    //  -displayComposerSheet
    // Change carriage returns to line feeds so they willl be recognized.		//leg20100701 - 1.0.2
    [body replaceOccurrencesOfString:@"\r" withString:@"\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [body length])];
    [picker setSubject:subject];
    [picker setMessageBody:body isHTML:NO];
	
	// Present the composer
    [self presentViewController:picker animated:YES completion:nil];            //leg20140205 - 1.2.7
	
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{       
#pragma unused (controller, error)

	NSString *resultMessage;
        // Notifies users about errors associated with the interface
        switch (result)
        {
                case MFMailComposeResultCancelled:
                        resultMessage = @"Message canceled.";
                        break;
                case MFMailComposeResultSaved:
                        resultMessage = @"Message saved.";
                        break;
                case MFMailComposeResultSent:
                        resultMessage = @"Message sent.";
                        break;
                case MFMailComposeResultFailed:
                        resultMessage = @"Message failed.";
                        break;
                default:
                        resultMessage = @"Message not sent.";
                        break;
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];                //leg20140205 - 1.2.7
		
		[self alertEmailStatus:resultMessage];
}

#pragma mark UIAlertView

- (void)alertEmailStatus:(NSString*)alertMessage
{
	// open an alert with just an OK button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email Status" message: alertMessage
							delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];	
}

- (void)alertOKCancelAction:(NSString*)alertMessage
{
	// open a alert with an OK and cancel button
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Email" message: alertMessage
							delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
	[alert show];
}


#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{	
#pragma unused (actionSheet)

	// use "buttonIndex" to decide your action
	//
	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		//NSLog(@"Cancel");
	} else {
		//NSLog(@"OK");
		[self sendEmail];
	}
}

#pragma mark UIActionSheet

- (void)dialogOKCancelAction
{
	// open a dialog with an OK and cancel button
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"UIActionSheet <title>"
									delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Proceed" otherButtonTitles:nil];
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view]; // show from our table view (pops up in the middle of the table)
}

//- (void)dismissWithClickedButtonIndex:(NSInteger)buttonIndex animated:(BOOL)animated


#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
#pragma unused (actionSheet)

	// the user clicked one of the OK/Cancel buttons
	if (buttonIndex == 0)
	{
		[self sendEmail];
	}
	else
	{
		;		// user tapped "Cancel" button
	}	
}

@end
