////
////  main.m
////  NotesTopX
////
////  Created by Lewis Garrett on 4/5/09.
////  Copyright Iota 2009. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//
//int main(int argc, char *argv[]) {
//    @autoreleasepool {
//        int retVal = UIApplicationMain(argc, argv, nil, nil);
//        return retVal;
//    }
//}

                                                                                //leg20210625 - TopxNotes 1.6
#import <UIKit/UIKit.h>
#import "TopXNotesAppDelegate.h"

int main(int argc, char * argv[]) {
    NSString * appDelegateClassName;
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        appDelegateClassName = NSStringFromClass([TopXNotesAppDelegate class]);
    }
    return UIApplicationMain(argc, argv, nil, appDelegateClassName);
}
